<?php

require_once("funciones.php");
require_once ("modelUsuario.php");


/* VALIDAR LOS DATOS DE ENTRADA */


if (empty( $_GET['id'])){
    header('Location:index.php');
}


$idVehiculo=$_GET["id"];
$mysqli = conectarBD();
$select = "SELECT * FROM Vehiculos WHERE idVehiculo = $idVehiculo ";
$result = $mysqli->query($select);
$rows = $result->fetch_array(MYSQLI_ASSOC);
$cant = $result->num_rows;
if ($cant == 0) {
  header('Location:index.php');

}



  include ('cabecera.php');

 ?>



<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
    <body>
    <!-- CABECERA DE LA PAGINA -->
    <header class="panel-heading container-fluid">

        <!-- Logo -->
        <div class="col-lg-12">
            <div class="col-lg-6">
                <a href="index.php">
                    <img src="imagenes/logo.png" alt="75" class="img-rounded"></a>
            </div>

            <!-- FORMULARIO DE LOGIN-->
            <?php
            if (!isset($_SESSION['usuario'])){
            ?>
            <div class="col-lg-6 left">
                <!-- Div contenedor del formulario de login flotando a derecha del logo -->
                <div class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block">
                    <div class="alert " id="divError" style="margin-bottom: inherit;"></div>
                    <!-- Formulario de login -->
                    <form class="form-inline" action="login.php" method="post" onsubmit="return validaLogin(this)"
                          name="formularioLogin" style="float: right;">
                        <!-- IMPRIME MENSAJES DE ERROR DESDE JS -->
                        <div class="form-group" id="divUsuario">
                            <label for="idUsuario"> Usuario&nbsp;</label>
                            <input type="text" id="idUsuario" name="Usuario" class="form-control"
                                   placeholder="Ingrese su Usuario" onclick="borrarError(document.getElementById('divError'));
                               borrarError(document.getElementById('errorLogin'));"/>
                        </div>
                        <div class="form-group" id="divPassword">
                            <label for="password">&nbsp;&nbsp;&nbsp;Clave&nbsp;</label>
                            <input type="password" id="password" name="Clave" class="form-control"
                                   placeholder="Ingrese Su Contraseña"
                                   onclick="borrarError(document.getElementById('divError'))">
                        </div>

                        <input type="submit" class="btn btn-default" value="Entrar"/>
                    </form>
                </div>
                <?php
                }
                if ((isset($_SESSION['exception'])) && $_SESSION['exception']->getMessage() != ""){
                    echo "<div id=\"errorLogin\"class=\"alert alert-danger col-lg-7\" style=\"margin-top: 0%;\">"
                        .$_SESSION['exception']->getMessage()."</div>";
                    unset($_SESSION['exception']);

                }
                ?>
            </div>
        </div>
    </header>
            	

          <center>
     
                  
                          <?php
                              
                              

                          ?>
             	 
           
       
           <div class="container-fluid">
                 <h1> <span class="label label-success">     <?php echo ucwords(getMarca($rows['idModelo'])); ?> <?php echo ucwords(getModelo($rows['idModelo'])); ?>    &nbsp;&nbsp;&nbsp;&nbsp; $ <?php echo $rows['precio']?> </h1></span> 
            </div>
         </center>

        

 
                
 <center>
 <div class="container-fluid"> <br><br>
      <div class="col-lg-12">   
          <div class="col-lg-4">
           <?php 
           

   echo'<img src="ima.php?id='.$idVehiculo.'" class="img-thumbnail" width=400 heigth=400 />'; 

?>


        </div> 
       
             <div class="col-lg-6">
                  <div id='panelDetalle' class="panel panel-default" >
                      <div class="panel-heading">
                        <h3 class="panel-title" style="width: 218px;">Datos de la publicacion</h3>
                      </div>

                      <div class="panel-body">
                        
                        <strong> <p>Tipo: <?php echo ucwords(getTipo($rows['idTipo'])); ?> </p></strong>
                        <strong> <p>Patente: <?php echo ucwords($rows['dominio']); ?> </p></strong>
                        <strong> <p>Año: <?php echo ucwords($rows['anio']); ?> </p></strong>


                     
                      </div>
                  </div>
            </div>
            <div class="col-lg-1"> <a action="alert();" class="btn btn-primary btn-lg " role="button">COMPRAR</a><br><br></div>
           
          
          </div>
          



    </div> 
      </div>        
</div>      
</center>

<br><br>

   
             
            
               
              <div id='panelDescripcion' class="panel panel-default">
                   <div class="panel-heading">
                      <h1 class="panel-title" style="width: 1218px;">Caracteristicas del vehiculo</h1>
                    </div>
                    <div class="panel-body">
                    <strong> <p><?php  ucwords(impCaracteristicas($idVehiculo)); ?></p></strong> 
                    
                    </div>
                       
     
              </div>        

    
    </body>
</html>