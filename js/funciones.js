/**
 * Created by simch on 6/9/2016.
 */

/*
Valida el formulario de login, y cambia 
 */
function validaLogin(){
    "use strict";
	var expresionMail = /^\w+([\.\-]?\w+)*@\w+([\.]?\w+)*(\.\w{2,4})+$/; //Expresion regular verifica mail
    var idUsuario = document.getElementById("idUsuario").value;
    var password = document.getElementById("password").value;
    if (idUsuario == null || idUsuario.length == 0 || idUsuario.indexOf(' ') == 0 || !expresionMail.test(idUsuario)){
        //Cambia el tipo de clase asociada a bootstrap para remarcar con rojo los bordes del campo
        setDivFormHasError(document.getElementById("divUsuario"));
        imprimirError(document.getElementById("divError"),"El usuario debe ser una cuenta de correo electronico");
        limpiarCampo(document.getElementById("idUsuario"));
        return false;
    }else if (password == null || password.length == 0 || password.indexOf(' ') == 0){
        setDivFormHasError(document.getElementById("divPassword"));
        imprimirError(document.getElementById("divError"),"La contraseña no puede comenzar por un espacio en blanco ni estar vacia");
        limpiarCampo(document.getElementById("password"));
        return false;
    }else {
        return true;
    }
}

/*
Valida el formulario de contacto
*/
function validaContact(){
    "use strict";
    var expresionMail = /^\w+([\.\-]?\w+)*@\w+([\.]?\w+)*(\.\w{2,4})+$/; //Expresion regular verifica mail
    var expresionLetras = /^[a-zA-Z][a-zA-Z]*/; //Expresion regular para verificar que solo sean letras
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var mail = document.getElementById("email").value;
    var mensaje = document.getElementById("consulta").value;
    var asunto = document.getElementById("idAsunto").value;

//Campo vacio o comienza con espacio blanco o contiene numeros
    if (nombre == null || nombre.length == 0 || nombre.indexOf(' ') == 0 || (!expresionLetras.test(nombre))){
        setDivFormHasError(document.getElementById("divNom"));
        setPlaceHolder(document.getElementById("nombre"),"El nombre no puede estar vacio, comenzar con" +
            " espacios en blanco o contener numeros");
       limpiarCampo(document.getElementById("nombre")); //Borra lo ingresado por el usuario
        imprimirError(document.getElementById("error"),"Nombre Vacio o Nulo");
        return false;
    }else
//Campo vacio, comienza con espacio blanco o contiene numeros
        if (apellido == null || apellido.length == 0 || apellido.indexOf(' ') == 0 ||
        (!expresionLetras.test(apellido))){
        setDivFormHasError(document.getElementById("divApe"));
        setPlaceHolder(document.getElementById("apellido"),"El apellido no puede estar vacio, comenzar con" +
            "espacios en blanco o contener numeros");
        limpiarCampo(document.getElementById("apellido")); //Borra lo ingresado por el usuario
            imprimirError(document.getElementById("error"),"Apellido Vacio o Nulo");
        return false;
    } else
//Campo vacio, comienza con espacio blanco, o no cumple con el formato de un email
        if (mail == null || mail.length == 0 || mail.indexOf(' ') == 0 || (!expresionMail.test(mail))){
        setDivFormHasError(document.getElementById("divEmail"));
        setPlaceHolder(document.getElementById("email"),"El email no debe estar vacio y debe ser de la forma" +
            "correo @ servidor . dominio ej: correo@gmail.com, correo@hotmail.com.ar");
        limpiarCampo(document.getElementById("email"));
            imprimirError(document.getElementById("error"),"Email Incorrecto o nulo");
        return false;
    } else
        if (asunto.length == 0 || asunto.indexOf(' ') == 0 || asunto == null) {
        setDivFormHasError(document.getElementById("divAsunto"));
        setPlaceHolder(document.getElementById("idAsunto"), "El Asunto no debe Estar vacio, comenzar con" +
            " un espacio blanco o contener numeros");
        limpiarCampo(document.getElementById("idAsunto"));
        imprimirError(document.getElementById("error"), "Asunto erroneo o vacio.");
        return false;
    }else
//Campo vacio o comienza con espacio en blanco
        if (mensaje.length == 0 || mensaje.indexOf(' ') == 0 || mensaje == null){
        setDivFormHasError(document.getElementById("divConsulta"));
        setPlaceHolder(document.getElementById("consulta"),"La consulta no debe esar vacia o comenzar con" +
            " un espacio blanco");
            limpiarCampo(document.getElementById("consulta"));
            imprimirError(document.getElementById("error"),"Consulta Vacia");
            return false;
    }
    return true;
}

/*
VALIDA FORMULARIO ALTA DE TIPO VEHICULO
 */
function validaAltaType(){
    var nombre = document.getElementById("nomTipo").value;
    var expresionLetras = /^[a-zA-Z][a-zA-Z]*/; //Expresion regular para verificar que solo sean letras
    if (nombre == null || nombre.length == 0 || nombre.indexOf(' ') == 0 || (!expresionLetras.test(nombre)) ){
        setDivFormHasError(document.getElementById("divNomTipo"));
        limpiarCampo(document.getElementById("nomTipo"));
        setPlaceHolder(document.getElementById("nomTipo"),"Este Campo debe contener solo letras, No comenzar por" +
            " un blanco, y No estar Vacio");
        imprimirError(document.getElementById("divError"),"Verifique que ingreso correctamente los datos solicitados");
        return false;
    }else {
        return true;
    }
}

/*
 VALIDA FORMULARIO DE BAJA DE TIPO VEHICULO
 */
function validaBajaType() {
    var select = document.getElementById("selTipo");
    if (select.selectedIndex <= 0) {
        setDivFormHasError(document.getElementById("divSelTipo"));
        imprimirError(document.getElementById("errorBajaType"),"Por Favor Seleccione una Marca");
        return false;
    }
    else {
        return true;
    }
}

/*
VALIDA FORMULARIO DE MODIFICACION DE TIPO VEHICULO
 */
function validaModyType(){
    var select = document.getElementById("selTipo");
    var newTipo = document.getElementById("nomTipo").value;
    var expTipo = /^[a-zA-Z][a-zA-Z]*/;//Expresion regular comienza con 1 letra obligatoriamente

    //Valida se haya seleccionado un tipo
    if (select.selectedIndex <= 0){
        setDivFormHasError(document.getElementById("divSelTipo"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione un Tipo de vehiculo a modificar");
        return false;
    }else if (newTipo == null || newTipo.length == 0 || newTipo.indexOf(' ') == 0 || (!expTipo.test(newTipo))){
        setDivFormHasError(document.getElementById("divNomTipo"));
        imprimirError(document.getElementById("divError"),"Por favor ingrese un Tipo Valido.");
        setPlaceHolder(document.getElementById("nomTipo"),"El tipo no debe comenzar por espacio, con numeros, o " +
            "estar vacio.");
        limpiarCampo(document.getElementById("nomTipo"));
        return false;
    }
    return true;
}

/*
VALIDA FORMULARIO ALTA DE CARACTERISTICA
 */
function validaNewCaracteristica() {
    var categoria = document.getElementById("idCaracteristica").value;
    var expresionLetras = /^[a-zA-Z][a-zA-Z]*/; //Expresion regular para verificar que solo sean letras
    if (categoria == null || categoria.length == 0 || categoria.indexOf(' ') == 0 ||
        (!expresionLetras.test(categoria))){
        setDivFormHasError(document.getElementById("divCaracteristica"));
        setPlaceHolder(document.getElementById("idCaracteristica"),"Este campo no puede estar vacio, comenzar por un " +
            "espacio blanco");
        limpiarCampo(document.getElementById("idCaracteristica"));
        imprimirError(document.getElementById("divError"),"Por favor revise el formulario.");
        return false;
    }else{
        return true;
    }
}

/*
Valida el formulario de baja de una caracteristica
 */
function validaBajaCaracteristica(){
    var select = document.getElementById("idCaracteristica");
    if (select.selectedIndex <= 0){
        setDivFormHasError(document.getElementById("divCaracteristica"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione una caracteristica a borrar");
        return false;
    }else{
        return true;
    }
}

/*
Valida formulario de modificacion de caracteristicas
 */
function validaModCaracteristica() {
    var select = document.getElementById("idCaracteristica");
    var nombre = document.getElementById("idNewCategoria").value;
    var expresionLetras = /^[a-zA-Z][a-zA-Z]*/; //Expresion regular para verificar que solo sean letras

    if (select.selectedIndex <= 0){ //Si no se selecciono una caracteristica existente
        setDivFormHasError(document.getElementById("divCaracteristica"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione una caracteristica a modificar");
        return false;
    } else if (nombre == null || nombre.length == 0 || nombre.indexOf(' ') == 0 || (!expresionLetras.test(nombre))){
        setDivFormHasError(document.getElementById("divNewCaracteristica"));
        setPlaceHolder(document.getElementById("idNewCategoria"),"Este campo no debe estar vacio, o comenzar por " +
            "espacio blanco.");
        limpiarCampo(document.getElementById("idNewCategoria"));
        imprimirError(document.getElementById("divError"),"Por favor verifique el nuevo nombre.");
        return false;
    }else{
        return true;
    }
}

/*
VALIDA FORMULARIO ALTA VEHICULO
 */
function validaNewVehiculo() {
    var modelo = document.getElementById("idModelo");
    var tipo = document.getElementById("idTipo");
    var dominio = document.getElementById("idDominio").value;
    var expDominio = /^[a-zA-Z0-9]{3}[A-Za-z0-9]{3}$/;
    var expAnio = /^[1-2][0-9]{3}$/; //Expresion para validar el año correctamente 4 digitos y no comenzando por 0
    var anio = document.getElementById("idAnio").value;
    var precio = document.getElementById("idPrecio").value;
    var anioAct = (new Date()).getFullYear(); //Tomo el año actual
    var imagen = document.getElementById("idImagen").value;
    var formato = /.(jpg|jpeg)$/;

    // Se evalua que se haya seleccionado un modelo de vehiculo
    if (tipo.selectedIndex <= 0){
        setDivFormHasError(document.getElementById("divTipo"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione un Tipo");
        return false;

        //Se evalua que se haya seleccionado un tipo de vehiculo
    }else if (modelo.selectedIndex <= 0){
    setDivFormHasError(document.getElementById("divModelo"));
    imprimirError(document.getElementById("divError"),"Por favor seleccione un modelo");
    return false;

        //Se evalua que el dominio cumpla los requisitos de: no ser vacio, no comenzar por Letras
        // O espacio blanco y cumpla el formato LLLNNN
    }else if (dominio == null || dominio.indexOf(' ') == 0 || dominio.length == 0 || (!expDominio.test(dominio))){
        setDivFormHasError(document.getElementById("divDominio"));
        imprimirError(document.getElementById("divError"),"Por favor Cargue la patente del vehiculo");
        return false;

        //Se evalua el año cumpla los requisitos 1850 < anio < año Actual,
        // no sea vacio, no comience por un espacio blanco
    }else if(anio == null || anio.indexOf(' ') == 0 || parseInt(anio)< 1850 || anio.length == 0 ||
        parseInt(anio) > anioAct || (!expAnio.test(anio))){
        setDivFormHasError(document.getElementById("divAnio"));
        setPlaceHolder(document.getElementById("idAnio"),"El año debe estar comprendido entre el 1850 y el año" +
            " Actual.");
        imprimirError(document.getElementById("divError"),"Por favor verifique el año del vehiculo, debe ingresar un " +
            "año valido");
        limpiarCampo(document.getElementById("idAnio"));
        return false;
    }else if (precio == null || precio == 0 || precio.length == 0){ //Valida el precio
        setDivFormHasError(document.getElementById("divPrecio"));
        setPlaceHolder(document.getElementById("idPrecio"),"Por favor introduzca un precio valido");
        limpiarCampo(document.getElementById("idPrecio"));
        imprimirError(document.getElementById("divError"),"Por favor introduzca un precio valido, distinto de 0");
        return false;
    } else if ( imagen.length == 0 ){
        setDivFormHasError(document.getElementById("divImagen"));
        imprimirError(document.getElementById("divError"),"Seleccione una imagen");
        return false;
    }else if ((!formato.test(imagen))){
        setDivFormHasError(document.getElementById("divImagen"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione una imagen JPEG o JPG");
        return false;
    }
    return true;
}

function validaModVehiculo() {
    var modelo = document.getElementById("idModelo");
    var tipo = document.getElementById("idTipo");
    var dominio = document.getElementById("idDominio").value;
    var expDominio = /^[a-zA-Z0-9]{3}[A-Za-z0-9]{3}$/;
    var expAnio = /^[1-2][0-9]{3}$/; //Expresion para validar el año correctamente 4 digitos y no comenzando por 0
    var anio = document.getElementById("idAnio").value;
    var precio = document.getElementById("idPrecio").value;
    var anioAct = (new Date()).getFullYear(); //Tomo el año actual
    var imagen = document.getElementById("idImagen").value;
    var formato = /.(jpg|jpeg)$/;

    // Se evalua que se haya seleccionado un modelo de vehiculo
    if (tipo.selectedIndex <= 0){
        setDivFormHasError(document.getElementById("divTipo"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione un Tipo");
        return false;

        //Se evalua que se haya seleccionado un tipo de vehiculo
    }else if (modelo.selectedIndex <= 0){
    setDivFormHasError(document.getElementById("divModelo"));
    imprimirError(document.getElementById("divError"),"Por favor seleccione un modelo");
    return false;

        //Se evalua que el dominio cumpla los requisitos de: no ser vacio, no comenzar por Letras
        // O espacio blanco y cumpla el formato LLLNNN
    }else if (dominio == null || dominio.indexOf(' ') == 0 || dominio.length == 0 || (!expDominio.test(dominio))){
        setDivFormHasError(document.getElementById("divDominio"));
        imprimirError(document.getElementById("divError"),"Por favor Cargue la patente del vehiculo");
        return false;

        //Se evalua el año cumpla los requisitos 1850 < anio < año Actual,
        // no sea vacio, no comience por un espacio blanco
    }else if(anio == null || anio.indexOf(' ') == 0 || parseInt(anio)< 1850 || anio.length == 0 ||
        parseInt(anio) > anioAct || (!expAnio.test(anio))){
        setDivFormHasError(document.getElementById("divAnio"));
        setPlaceHolder(document.getElementById("idAnio"),"El año debe estar comprendido entre el 1850 y el año" +
            " Actual.");
        imprimirError(document.getElementById("divError"),"Por favor verifique el año del vehiculo, debe ingresar un " +
            "año valido");
        limpiarCampo(document.getElementById("idAnio"));
        return false;
    }else if (precio == null || precio == 0 || precio.length == 0){ //Valida el precio
        setDivFormHasError(document.getElementById("divPrecio"));
        setPlaceHolder(document.getElementById("idPrecio"),"Por favor introduzca un precio valido");
        limpiarCampo(document.getElementById("idPrecio"));
        imprimirError(document.getElementById("divError"),"Por favor introduzca un precio valido, distinto de 0");
        return false;
    } 
        return true;
}

/*
VALIDA EL FORMULARIO DE ALTA DE NUEVA MARCA SI EN EL SELECT DE MARCA SE ENCUENTRA SELECCIONADA LA OPCION
OTRA MARCA, MUESTRA EL INPUT NUEVA MARCA, DE LO CONTRARIO LO OCULTA
 */
function validaNewMarcaModel() {
    var selectMarca = document.getElementById("selectMarca");
    var newModel = document.getElementById("nomModelo").value;
    var newMarca = document.getElementById("nomMarca").value;

    //si se selecciono OTRA MARCA VALIDO EL CAMPO MARCA
    if (selectMarca.selectedIndex <= 0){ //Si otra marca => muestro input Nueva Marca y valido ambos input
        if (newMarca == null || newMarca.length == 0 || newMarca.indexOf(' ') == 0 ){
            setDivFormHasError(document.getElementById("divNomMarca"));
            setPlaceHolder(document.getElementById("nomMarca"),"La marca no puede comenzar por un espacio blanco " +
                "o estar vacio");
            limpiarCampo(document.getElementById("nomMarca"));
            imprimirError(document.getElementById("error"),"Marca erronea o Nula");
            return false;
        }else if (newModel == 0 || newModel.length == 0 || newModel.indexOf(' ') == 0 ){ //La marca es valida pero no el modelo
            setDivFormHasError(document.getElementById("divModelo"));
            limpiarCampo(document.getElementById("nomModelo"));
            setPlaceHolder(document.getElementById("nomModelo"),"El modelo no puede comenzar por un espacio vacio" +
                " o estar vacio.");
            imprimirError(document.getElementById("error"),"Modelo erroneo o Nulo");
            return false;
        }
    }else{ //Si != Otra Marca => oculto input y solo valido modelo
        if (newModel == 0 || newModel.length == 0 || newModel.indexOf(' ') == 0){
            setDivFormHasError(document.getElementById("divModelo"));
            setPlaceHolder(document.getElementById("nomModelo"),"El Modelo no puede estar vacio o comenzar" +
                " por espacio blanco");
            limpiarCampo(document.getElementById("nomModelo"));
            imprimirError(document.getElementById("error"),"Modelo erroneo o Nulo");
            return false;
        }else {
            return true;
        }
    }
}

/*
VALIDA FORMULARIO DE MODIFICACION DE MARCA Y MODELO
 */

/*
Valida el formulario de Baja de Marca y Modelo
 */
function validaBajaMyM(){
    var marca = document.getElementById("selMarca");
    var modelo = document.getElementById("selMod");
    if (marca.selectedIndex == 0){
        setDivFormHasError(document.getElementById("divSelMarca"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione una Marca");
        return false;
    }else if (modelo.selectedIndex == 0){
        setDivFormHasError(document.getElementById("divSelMod"));
        imprimirError(document.getElementById("divError"),"Por favor seleccione un modelo");
        return false
    }else{
        return true;
    }
}

/*
VALIDA FORMULARIO DE MODIFICACION DE MARCA Y MODELO
 */
function validaModiMarca() {
    var selMarca = document.getElementById("selMarca");
    var selMod = document.getElementById("selNomMod");
    var newMarca = document.getElementById("nomMarca").value;
    var newModel = document.getElementById("nomModelo").value;
    //Si Se selecciona Opcion de solo modificar modelo y Solo modificar Marca en simultaneo:
    if (selMarca.selectedIndex <= 0 && selMod.selectedIndex <= 0){
        //Imprimo error de que se realize una de las 2 acciones
        imprimirError(document.getElementById("divError"),"Se debe seleccionar al menos 1 modelo a modificar o 1 marca" +
            " a modificar");
        setDivFormHasError(document.getElementById("divSelMarca"));
        setDivFormHasError(document.getElementById("divSelMod"));
        return false;
    }else if (selMod.selectedIndex <= 0){
        //Si Solo se desea modificar una marca Valido solo el campo de marca
        if (newMarca == null || newMarca.length == 0 || newMarca.indexOf(' ') == 0){
            setDivFormHasError(document.getElementById("divNewMarca"));
            imprimirError(document.getElementById("divError"),"La marca no puede comenzar por espacios blancos u estar vacia");
            setPlaceHolder(document.getElementById("nomMarca"),"Por favor ingrese una marca");
            limpiarCampo(document.getElementById("nomMarca"));
            return false;
        }
    }else if (selMarca.selectedIndex <= 0) {
        //Si solo se Desea modificar el modelo valido solo el campo de modelo
        if (newModel == null || newModel.indexOf(" ") == 0 || newModel.length == 0) {
            setDivFormHasError(document.getElementById("divNewModel"));
            imprimirError(document.getElementById("divError"), "El modelo no puede comenzar por  espacios blancos u estar vacia");
            setPlaceHolder(document.getElementById("nomModelo"), "Por favor ingrese un Modelo");
            limpiarCampo(document.getElementById("nomModelo"));
            return false;
        }
    } else{ //Si se desea modificar Marca y Modelo
        //Si Solo se desea modificar una marca Valido solo el campo de marca
        if (newMarca == null || newMarca.length == 0 || newMarca.indexOf(' ') == 0){
            setDivFormHasError(document.getElementById("divNewMarca"));
            imprimirError(document.getElementById("divError"),"La marca no puede comenzar por espacios blancos u estar vacia");
            setPlaceHolder(document.getElementById("nomMarca"),"Por favor ingrese una marca");
            limpiarCampo(document.getElementById("nomMarca"));
            return false;
        }else if (newModel == null || newModel.indexOf(" ") == 0 || newModel.length == 0){
            setDivFormHasError(document.getElementById("divNewModel"));
            imprimirError(document.getElementById("divError"), "El modelo no puede comenzar por  espacios blancos u estar vacia");
            setPlaceHolder(document.getElementById("nomModelo"), "Por favor ingrese un Modelo");
            limpiarCampo(document.getElementById("nomModelo"));
            return false;
        }
    }
    return true;
}
/*
OCULTA Y MUESTRA EL CAMPO PASADO POR PARAMETRO SI EL SELECT PASADO POR PARAMETRO ES DISTITNO DEL SELECTED
 */
function muestraOcultaInput(sel,inp){
    if (sel.selectedIndex <= 0){
        inp.style.display = "block";
    }else{
        inp.style.display = "none";
    }


}

/*
 MUESTRA EL CAMPO POR PARAMETRO SI EL SELECT PASADO POR PARAMETRO ES DISTITNO DEL SELECTED
 */
function ocultaMuestraInput(sel,inp){
    if (sel.selectedIndex > 0){
        inp.style.display = "block";
    }else{
        inp.style.display = "none";
    }
}
/*
Borra lo ingresado por el usuario
 */
function limpiarCampo(campo){
    campo.value = null;
}

/*
Asigna al div pasado por parametro la case css form-group y has-error

 */
function setDivFormHasError(thisDiv){
    (thisDiv).className = "form-group has-error";
}

/*
Asigna al div pasado por parametro la clase css form-group
 */
function setDivFormGroup(idDiv) {
    (idDiv).className = "form-group";
}


/*
Imprime en el div destinado a errores Pasado por parametro los mensajes de error llegados como parametros
 */
function imprimirError(div,error){
    div.innerHTML = error;
    div.className = "alert alert-danger";
}

/*
Borra el div destinado a errores
 */
function borrarError(div){
    div.innerHTML = "";
    div.className = "";
}
/*
Setea el placeHolder de un elemento pasado por parametro
 */
function setPlaceHolder(thisInput,message) {
    "use strict";
    thisInput.placeholder = message;
}

/*
MUESTRA UN CARTEL DE CONFIRMACION, SI SE ACEPTA, ES REENVIADO A LA DIRECCION PASADA POR PARAMETRO
Se utiliza desde php en la pagina de listadoBajaVehiculo y listadoModiVehiculo
 */
function cartelConfirmacion(pagina) {
    if (confirm("Seguro que desea realizar la operacion?")){
        document.location.href = pagina;
    }
}

/* FUNCION DE ORDENAMIENTO*/

var stIsIE = /*@cc_on!@*/false;

sorttable = {
  init: function() {
    // quit if this function has already been called
    if (arguments.callee.done) return;
    // flag this function so we don't do the same thing twice
    arguments.callee.done = true;
    // kill the timer
    if (_timer) clearInterval(_timer);

    if (!document.createElement || !document.getElementsByTagName) return;

    sorttable.DATE_RE = /^(\d\d?)[\/\.-](\d\d?)[\/\.-]((\d\d)?\d\d)$/;

    forEach(document.getElementsByTagName('table'), function(table) {
      if (table.className.search(/\bsortable\b/) != -1) {
        sorttable.makeSortable(table);
      }
    });

  },

  makeSortable: function(table) {
    if (table.getElementsByTagName('thead').length == 0) {
      // table doesn't have a tHead. Since it should have, create one and
      // put the first table row in it.
      the = document.createElement('thead');
      the.appendChild(table.rows[0]);
      table.insertBefore(the,table.firstChild);
    }
    // Safari doesn't support table.tHead, sigh
    if (table.tHead == null) table.tHead = table.getElementsByTagName('thead')[0];

    if (table.tHead.rows.length != 1) return; // can't cope with two header rows

    // Sorttable v1 put rows with a class of "sortbottom" at the bottom (as
    // "total" rows, for example). This is B&R, since what you're supposed
    // to do is put them in a tfoot. So, if there are sortbottom rows,
    // for backwards compatibility, move them to tfoot (creating it if needed).
    sortbottomrows = [];
    for (var i=0; i<table.rows.length; i++) {
      if (table.rows[i].className.search(/\bsortbottom\b/) != -1) {
        sortbottomrows[sortbottomrows.length] = table.rows[i];
      }
    }
    if (sortbottomrows) {
      if (table.tFoot == null) {
        // table doesn't have a tfoot. Create one.
        tfo = document.createElement('tfoot');
        table.appendChild(tfo);
      }
      for (var i=0; i<sortbottomrows.length; i++) {
        tfo.appendChild(sortbottomrows[i]);
      }
      delete sortbottomrows;
    }

    // work through each column and calculate its type
    headrow = table.tHead.rows[0].cells;
    for (var i=0; i<headrow.length; i++) {
      // manually override the type with a sorttable_type attribute
      if (!headrow[i].className.match(/\bsorttable_nosort\b/)) { // skip this col
        mtch = headrow[i].className.match(/\bsorttable_([a-z0-9]+)\b/);
        if (mtch) { override = mtch[1]; }
          if (mtch && typeof sorttable["sort_"+override] == 'function') {
            headrow[i].sorttable_sortfunction = sorttable["sort_"+override];
          } else {
            headrow[i].sorttable_sortfunction = sorttable.guessType(table,i);
          }
          // make it clickable to sort
          headrow[i].sorttable_columnindex = i;
          headrow[i].sorttable_tbody = table.tBodies[0];
          dean_addEvent(headrow[i],"click", sorttable.innerSortFunction = function(e) {

          if (this.className.search(/\bsorttable_sorted\b/) != -1) {
            // if we're already sorted by this column, just
            // reverse the table, which is quicker
            sorttable.reverse(this.sorttable_tbody);
            this.className = this.className.replace('sorttable_sorted',
                                                    'sorttable_sorted_reverse');
            this.removeChild(document.getElementById('sorttable_sortfwdind'));
            sortrevind = document.createElement('span');
            sortrevind.id = "sorttable_sortrevind";
            sortrevind.innerHTML = stIsIE ? '&nbsp<font face="webdings">5</font>' : '';
            this.appendChild(sortrevind);
            return;
          }
          if (this.className.search(/\bsorttable_sorted_reverse\b/) != -1) {
            // if we're already sorted by this column in reverse, just
            // re-reverse the table, which is quicker
            sorttable.reverse(this.sorttable_tbody);
            this.className = this.className.replace('sorttable_sorted_reverse',
                                                    'sorttable_sorted');
            this.removeChild(document.getElementById('sorttable_sortrevind'));
            sortfwdind = document.createElement('span');
            sortfwdind.id = "sorttable_sortfwdind";
            sortfwdind.innerHTML = stIsIE ? '&nbsp<font face="webdings">6</font>' : '';
            this.appendChild(sortfwdind);
            return;
          }

          // remove sorttable_sorted classes
          theadrow = this.parentNode;
          forEach(theadrow.childNodes, function(cell) {
            if (cell.nodeType == 1) { // an element
              cell.className = cell.className.replace('sorttable_sorted_reverse','');
              cell.className = cell.className.replace('sorttable_sorted','');
            }
          });
          sortfwdind = document.getElementById('sorttable_sortfwdind');
          if (sortfwdind) { sortfwdind.parentNode.removeChild(sortfwdind); }
          sortrevind = document.getElementById('sorttable_sortrevind');
          if (sortrevind) { sortrevind.parentNode.removeChild(sortrevind); }

          this.className += ' sorttable_sorted';
          sortfwdind = document.createElement('span');
          sortfwdind.id = "sorttable_sortfwdind";
          sortfwdind.innerHTML = stIsIE ? '&nbsp<font face="webdings">6</font>' : '';
          this.appendChild(sortfwdind);

            // build an array to sort. This is a Schwartzian transform thing,
            // i.e., we "decorate" each row with the actual sort key,
            // sort based on the sort keys, and then put the rows back in order
            // which is a lot faster because you only do getInnerText once per row
            row_array = [];
            col = this.sorttable_columnindex;
            rows = this.sorttable_tbody.rows;
            for (var j=0; j<rows.length; j++) {
              row_array[row_array.length] = [sorttable.getInnerText(rows[j].cells[col]), rows[j]];
            }
            /* If you want a stable sort, uncomment the following line */
            //sorttable.shaker_sort(row_array, this.sorttable_sortfunction);
            /* and comment out this one */
            row_array.sort(this.sorttable_sortfunction);

            tb = this.sorttable_tbody;
            for (var j=0; j<row_array.length; j++) {
              tb.appendChild(row_array[j][1]);
            }

            delete row_array;
          });
        }
    }
  },

  guessType: function(table, column) {
    // guess the type of a column based on its first non-blank row
    sortfn = sorttable.sort_alpha;
    for (var i=0; i<table.tBodies[0].rows.length; i++) {
      text = sorttable.getInnerText(table.tBodies[0].rows[i].cells[column]);
      if (text != '') {
        if (text.match(/^-?[£$¤]?[\d,.]+%?$/)) {
          return sorttable.sort_numeric;
        }
        // check for a date: dd/mm/yyyy or dd/mm/yy
        // can have / or . or - as separator
        // can be mm/dd as well
        possdate = text.match(sorttable.DATE_RE)
        if (possdate) {
          // looks like a date
          first = parseInt(possdate[1]);
          second = parseInt(possdate[2]);
          if (first > 12) {
            // definitely dd/mm
            return sorttable.sort_ddmm;
          } else if (second > 12) {
            return sorttable.sort_mmdd;
          } else {
            // looks like a date, but we can't tell which, so assume
            // that it's dd/mm (English imperialism!) and keep looking
            sortfn = sorttable.sort_ddmm;
          }
        }
      }
    }
    return sortfn;
  },

  getInnerText: function(node) {
    // gets the text we want to use for sorting for a cell.
    // strips leading and trailing whitespace.
    // this is *not* a generic getInnerText function; it's special to sorttable.
    // for example, you can override the cell text with a customkey attribute.
    // it also gets .value for <input> fields.

    if (!node) return "";

    hasInputs = (typeof node.getElementsByTagName == 'function') &&
                 node.getElementsByTagName('input').length;

    if (node.getAttribute("sorttable_customkey") != null) {
      return node.getAttribute("sorttable_customkey");
    }
    else if (typeof node.textContent != 'undefined' && !hasInputs) {
      return node.textContent.replace(/^\s+|\s+$/g, '');
    }
    else if (typeof node.innerText != 'undefined' && !hasInputs) {
      return node.innerText.replace(/^\s+|\s+$/g, '');
    }
    else if (typeof node.text != 'undefined' && !hasInputs) {
      return node.text.replace(/^\s+|\s+$/g, '');
    }
    else {
      switch (node.nodeType) {
        case 3:
          if (node.nodeName.toLowerCase() == 'input') {
            return node.value.replace(/^\s+|\s+$/g, '');
          }
        case 4:
          return node.nodeValue.replace(/^\s+|\s+$/g, '');
          break;
        case 1:
        case 11:
          var innerText = '';
          for (var i = 0; i < node.childNodes.length; i++) {
            innerText += sorttable.getInnerText(node.childNodes[i]);
          }
          return innerText.replace(/^\s+|\s+$/g, '');
          break;
        default:
          return '';
      }
    }
  },

  reverse: function(tbody) {
    // reverse the rows in a tbody
    newrows = [];
    for (var i=0; i<tbody.rows.length; i++) {
      newrows[newrows.length] = tbody.rows[i];
    }
    for (var i=newrows.length-1; i>=0; i--) {
       tbody.appendChild(newrows[i]);
    }
    delete newrows;
  },

  /* sort functions
     each sort function takes two parameters, a and b
     you are comparing a[0] and b[0] */
  sort_numeric: function(a,b) {
    aa = parseFloat(a[0].replace(/[^0-9.-]/g,''));
    if (isNaN(aa)) aa = 0;
    bb = parseFloat(b[0].replace(/[^0-9.-]/g,''));
    if (isNaN(bb)) bb = 0;
    return aa-bb;
  },
  sort_alpha: function(a,b) {
    if (a[0]==b[0]) return 0;
    if (a[0]<b[0]) return -1;
    return 1;
  },
  sort_ddmm: function(a,b) {
    mtch = a[0].match(sorttable.DATE_RE);
    y = mtch[3]; m = mtch[2]; d = mtch[1];
    if (m.length == 1) m = '0'+m;
    if (d.length == 1) d = '0'+d;
    dt1 = y+m+d;
    mtch = b[0].match(sorttable.DATE_RE);
    y = mtch[3]; m = mtch[2]; d = mtch[1];
    if (m.length == 1) m = '0'+m;
    if (d.length == 1) d = '0'+d;
    dt2 = y+m+d;
    if (dt1==dt2) return 0;
    if (dt1<dt2) return -1;
    return 1;
  },
  sort_mmdd: function(a,b) {
    mtch = a[0].match(sorttable.DATE_RE);
    y = mtch[3]; d = mtch[2]; m = mtch[1];
    if (m.length == 1) m = '0'+m;
    if (d.length == 1) d = '0'+d;
    dt1 = y+m+d;
    mtch = b[0].match(sorttable.DATE_RE);
    y = mtch[3]; d = mtch[2]; m = mtch[1];
    if (m.length == 1) m = '0'+m;
    if (d.length == 1) d = '0'+d;
    dt2 = y+m+d;
    if (dt1==dt2) return 0;
    if (dt1<dt2) return -1;
    return 1;
  },

  shaker_sort: function(list, comp_func) {
    // A stable sort function to allow multi-level sorting of data
    // see: http://en.wikipedia.org/wiki/Cocktail_sort
    // thanks to Joseph Nahmias
    var b = 0;
    var t = list.length - 1;
    var swap = true;

    while(swap) {
        swap = false;
        for(var i = b; i < t; ++i) {
            if ( comp_func(list[i], list[i+1]) > 0 ) {
                var q = list[i]; list[i] = list[i+1]; list[i+1] = q;
                swap = true;
            }
        } // for
        t--;

        if (!swap) break;

        for(var i = t; i > b; --i) {
            if ( comp_func(list[i], list[i-1]) < 0 ) {
                var q = list[i]; list[i] = list[i-1]; list[i-1] = q;
                swap = true;
            }
        } // for
        b++;

    } // while(swap)
  }
}



/* for Mozilla/Opera9 */
if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", sorttable.init, false);
}

/* for Internet Explorer */
/*@cc_on @*/
/*@if (@_win32)
    document.write("<script id=__ie_onload defer src=javascript:void(0)><\/script>");
    var script = document.getElementById("__ie_onload");
    script.onreadystatechange = function() {
        if (this.readyState == "complete") {
            sorttable.init(); // call the onload handler
        }
    };
/*@end @*/

/* for Safari */
if (/WebKit/i.test(navigator.userAgent)) { // sniff
    var _timer = setInterval(function() {
        if (/loaded|complete/.test(document.readyState)) {
            sorttable.init(); // call the onload handler
        }
    }, 10);
}

/* for other browsers */
window.onload = sorttable.init;

// written by Dean Edwards, 2005
// with input from Tino Zijdel, Matthias Miller, Diego Perini

// http://dean.edwards.name/weblog/2005/10/add-event/

function dean_addEvent(element, type, handler) {
    if (element.addEventListener) {
        element.addEventListener(type, handler, false);
    } else {
        // assign each event handler a unique ID
        if (!handler.$$guid) handler.$$guid = dean_addEvent.guid++;
        // create a hash table of event types for the element
        if (!element.events) element.events = {};
        // create a hash table of event handlers for each element/event pair
        var handlers = element.events[type];
        if (!handlers) {
            handlers = element.events[type] = {};
            // store the existing event handler (if there is one)
            if (element["on" + type]) {
                handlers[0] = element["on" + type];
            }
        }
        // store the event handler in the hash table
        handlers[handler.$$guid] = handler;
        // assign a global event handler to do all the work
        element["on" + type] = handleEvent;
    }
};
// a counter used to create unique IDs
dean_addEvent.guid = 1;

function removeEvent(element, type, handler) {
    if (element.removeEventListener) {
        element.removeEventListener(type, handler, false);
    } else {
        // delete the event handler from the hash table
        if (element.events && element.events[type]) {
            delete element.events[type][handler.$$guid];
        }
    }
};

function handleEvent(event) {
    var returnValue = true;
    // grab the event object (IE uses a global event object)
    event = event || fixEvent(((this.ownerDocument || this.document || this).parentWindow || window).event);
    // get a reference to the hash table of event handlers
    var handlers = this.events[event.type];
    // execute each event handler
    for (var i in handlers) {
        this.$$handleEvent = handlers[i];
        if (this.$$handleEvent(event) === false) {
            returnValue = false;
        }
    }
    return returnValue;
};

function fixEvent(event) {
    // add W3C standard event methods
    event.preventDefault = fixEvent.preventDefault;
    event.stopPropagation = fixEvent.stopPropagation;
    return event;
};
fixEvent.preventDefault = function() {
    this.returnValue = false;
};
fixEvent.stopPropagation = function() {
  this.cancelBubble = true;
}

// Dean's forEach: http://dean.edwards.name/base/forEach.js
/*
    forEach, version 1.0
    Copyright 2006, Dean Edwards
    License: http://www.opensource.org/licenses/mit-license.php
*/

// array-like enumeration
if (!Array.forEach) { // mozilla already supports this
    Array.forEach = function(array, block, context) {
        for (var i = 0; i < array.length; i++) {
            block.call(context, array[i], i, array);
        }
    };
}

// generic enumeration
Function.prototype.forEach = function(object, block, context) {
    for (var key in object) {
        if (typeof this.prototype[key] == "undefined") {
            block.call(context, object[key], key, object);
        }
    }
};

// character enumeration
String.forEach = function(string, block, context) {
    Array.forEach(string.split(""), function(chr, index) {
        block.call(context, chr, index, string);
    });
};

// globally resolve forEach enumeration
var forEach = function(object, block, context) {
    if (object) {
        var resolve = Object; // default
        if (object instanceof Function) {
            // functions have a "length" property
            resolve = Function;
        } else if (object.forEach instanceof Function) {
            // the object implements a custom forEach method so use that
            object.forEach(block, context);
            return;
        } else if (typeof object == "string") {
            // the object is a string
            resolve = String;
        } else if (typeof object.length == "number") {
            // the object is array-like
            resolve = Array;
        }
        resolve.forEach(object, block, context);
    }
};
