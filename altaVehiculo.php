<?php
/* Si no se inicio sesion, se redirije a indexPublico.php */
    require_once ('cabecera.php');
    require_once('funciones.php');
    checkLogin();
?>
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar-left">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default active" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <div style="width: 80%;" class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <?php
            if (empty($_POST)) {
                ?>
                <h4 class="text-primary">
                    Dar de Alta un Vehiculo
                </h4>
                <div class="has-error" id="divError"></div> <!-- DIV DONDE APARECE EL ERROR -->

                                                            <!-- FORMULARIO NUEVA CARACTERISTICA (SE REENVIA ASI MISMO EL FORMULARIO-->
                <form class="form-horizontal" name= "formNewVehiculo" action="altaVehiculo.php" method="post"
                      enctype="multipart/form-data"  onsubmit="return validaNewVehiculo()">

                    <!-- SELECT Tipo de Vehiculo -->
                    <div class="form-group" id="divTipo" onclick="setDivFormGroup(this)">
                        <label for="idTipo"> Seleccione Tipo de vehiculo:</label>
                        <select id="idTipo" name="tipo" class="form-control">
                            <option selected value="0">Seleccione un tipo de vehiculo</option>
                            <?php
                                selectTipo();
                            ?>
                        </select>
                    </div>

                    <!-- Campo select de modelo -->
                    <div class="form-group" id="divModelo" onclick="setDivFormGroup(this)">
                        <label for="idModelo">Seleccion Modelo</label>
                        <select id="idModelo" name="modelo" class="form-control">
                            <option selected value="ninguno">Ninguno</option>
                            <?php
                            selectModelo();
                            ?>
                        </select>
                    </div>

                    <!-- Input DOMINIO AUTOMOTOR -->
                    <div class="form-group" id="divDominio" onclick="setDivFormGroup(this)">
                        <label for="idDominio">Dominio Vehiculo:</label>
                        <input type="text" id="idDominio" name="dominio" placeholder="Ingrese el dominio del vehiculo"
                               class="form-control">
                    </div>

                    <!-- INPUT AÑO DEL VEHICULO -->
                    <div class="form-group" id="divAnio" onclick="setDivFormGroup(this)">
                        <label for="idAnio">Año del Vehiculo:</label>
                        <input type="text" id="idAnio" name="anio" placeholder="Ingrese el año del vehiculo"
                               class="form-control">
                    </div>

                    <!-- INPUT PRECIO DEL VEHICULO -->
                    <div class="form-group" id="divPrecio" onclick="setDivFormGroup(this)">
                        <label for="idPrecio">Precio del Vehiculo:</label>
                        <input type="number" id="idPrecio" name="precio" placeholder="Ingrese el precio del vehiculo"
                               class="form-control">
                    </div>

                    <!-- Caracteristicas -->
                    <div class="form-control-static" id="divCaracteristicas" onclick="setDivFormGroup(this);
                        borrarError(document.getElementById('divError')">
                        <?php //Carga todos los checkbox dinamicamente
                        cargaCaracteristicasHTML();
                        ?>
                    </div>
                    <!-- INPUT IMAGEN -->
                    <div class="form-group" id="divImagen" onclick="setDivFormGroup(this)">
                        <label for="idImagen">Agregar Imagen del vehiculo:</label>
                        <input type="file" id="idImagen" name="imagen">
                        <p class="text-left help-block">Formatos admitidos JPEG y JPG</p>
                    </div>

                    <input type="submit" class="btn btn-default" value="Enviar">
                </form>
                <?php
            }else{
                //SI ESTAN DEFINIDOS TODOS LOS INPUT, ESTA DEFINIDO EL VECTOR DE CARACTERISTICAS
                //Y el tipo de imagen es correcto
                if ((isset($_POST['modelo'])) && (isset($_POST['tipo'])) && (isset($_POST['dominio'])) &&
                    (isset($_POST['anio'])) && (isset($_POST['precio']))  && (isset($_FILES['imagen'])) &&
                    (validoTipoImagen($_FILES['imagen']) === true)){
                    $modelo = $_POST['modelo'];
                    $tipo = $_POST['tipo'];
                    $dominio = strtolower($_POST['dominio']);
                    $anio = $_POST['anio'];
                    $precio = $_POST['precio'];
                    if (isset($_POST['caracteristicaVehiculo'])) {
                        $caracteristicas = $_POST['caracteristicaVehiculo'];
                    }else{
                        $caracteristicas = array();
                    }
                    $foto = $_FILES['imagen'];
                    altaVehiculo($modelo,$tipo,$dominio,$anio,$precio,$caracteristicas,$foto);
                }else{
                    errorMessage("Por favor verifique los datos y vuelva a intentarlo");
                }
            }
        ?>
    </div>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>