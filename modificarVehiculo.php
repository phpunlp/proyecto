<?php
require_once "conexion.php";
require_once "funciones.php";
require_once "cabecera.php";
checkLogin();

if (isset($_GET['idVehiculo'])){
    if ((isset($_POST['modelo'])) && (isset($_POST['tipo'])) && (isset($_POST['dominio'])) &&
        (isset($_POST['anio'])) && (isset($_POST['precio']))
    ) {
        if (isset($_POST['caracteristicaVehiculos'])){
            $caracteristicas = array();
        }else{
            $caracteristicas = $_POST['caracteristicaVehiculo'];
        }
        if ($_FILES['imagen']['size'] === 0) { //Si no hay una nueva imagen cargada hago un update sin modificar la imagen
            updateSinImagen($_GET['idVehiculo'], $_POST['tipo'], $_POST['modelo'], $_POST['dominio'], $_POST['anio'],
                $_POST['precio'],$caracteristicas);
        } else {
            $newImage = $_FILES['imagen'];
            $tipoImagen = validoTipoImagen($newImage);
            if ($tipoImagen != false) {
                $tipoImagen = retornaTipoImagen($newImage);
                $binario_imagen = obtenerBinarioIMG($newImage);
                updateConImagen($_GET['idVehiculo'], $_POST['tipo'], $_POST['modelo'], $_POST['dominio'],
                    $_POST['anio'], $_POST['precio'], $binario_imagen, $tipoImagen,$caracteristicas);

            }
        }
    }
}