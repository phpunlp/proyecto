<?php
/**
 * Created by PhpStorm.
 * User: simch
 * Date: 10/10/2016
 * Time: 6:37 PM
 *
 * Debe eliminar de la BD el vehiculo tomado con GET y mostrar un mensaje confirmando la accion en caso que se
 * haya podido borrar el vehiculo o informar del error.
 * Si no se inicio sesion, se redirige al index (Para prevenir eliminaciones no deseadas
 */
require_once ('funciones.php');
session_start();
checkLogin();
//Si esta definido idVehiculo elimino el vehiculo de la BD, y retorno un ok, a la pagina listadoBajaVehiculo
if (!empty($_GET['idVehiculo'])){
    $idVehiculo = $_GET['idVehiculo'];
    //Consulta para saber si hay caracteristicas asociadas a dicho vehiculo
    $queryCat = "SELECT * FROM Vehiculos_Caracteristicas WHERE idVehiculo = '$idVehiculo'";
    //Consulta que elimina las caracteristicas relacionadas a dicho vehiculo
    $deleteCat = "DELETE FROM Vehiculos_Caracteristicas WHERE idVehiculo = '$idVehiculo'";
    //Consulta sobre el vehiculo
    $queryVehiculo = "SELECT * FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    //Consulta elimina vehiculo
    $deleteVehiculo = "DELETE FROM Vehiculos WHERE idVehiculo='$idVehiculo'";

    $mysqli = conectarBD();
    //Si el vehiculo existe
    if ($mysqli->query($queryVehiculo)->num_rows > 0){
        if ($mysqli->query($queryCat)->num_rows > 0){ //Si existen categorias asociadas
            $mysqli->query($deleteCat); //Elimino las categorias asociadas al idVehiculo de vehiculos_caracteristicas
        }
        $mysqli->query($deleteVehiculo); //Elimino el vehiculo
        $mysqli->close();
        header('location:listadoBajaVehiculo.php?error='.true.'&idVehiculo='.$idVehiculo);
    }else{
        $mysqli->close();
        header('location:listadoBajaVehiculo.php?error='.false.'&idVehiculo='.$idVehiculo);
    }
}