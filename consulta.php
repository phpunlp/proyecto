<?php 
function getConsulta($tipo, $modelo, $caracteristicas){


/* DECLARO LAS DOS VARIABLES QUE VOY A UTILIAR $CANT VA A CONTAR LA CANTIDAD DE CARACTERISTICAS QUE ME LLEGAN Y FILTROS VA A CREAR UN STRING CON LOS ID DE LAS CARACTERISTICAS */  
  $cant = 0;
  $filtros=''; 

/* RECIBO EL MODELO Y DEFINO LA VARIABLE $SETMOD SI SE ESPECIFICO ALGUN MODELO EN ESPECIAL */
  
  if ($modelo != 'Todos') {
     $setMod = True;
  }

/* RECIBO EL TIPO Y DEFINO LA VARIABLE $SETTIPO SI SE ESPECIFICO ALGUN TIPO EN ESPECIAL */
  
  if ($tipo != 'Todos') {
     $setTipo = True;
  }
  
/* EN ESTE FRAGMENTO VOY A RECIBIR LAS CARACTERISTICAS Y VOY A IR AGREGANDO LOS ID DE LAS MISMAS EN LA VARIABLE FILTROS QUE DECLARE PREVIAMENTE. Y EN CANT VOY A CONTAR LA CANTIDAD DE CARACTERISTICAS QUE LLEGAN */
  if (isset($caracteristicas)) {
    foreach($caracteristicas as $indice) { 
    if ($indice!='') { 
        $filtros.="'$indice', ";
        $cant= $cant + 1;

                        }
  } 
  
  /* LE CORTO LA ULTIMA COMA QUE ESTA DE MAS */
  $filtros=substr($filtros,0,strlen($filtros)-2);
  }
  
  /* ACA COMIENZO A PREGUNTAR POR LAS VARIABLES QUE ESTAN DEFINIDAS PARA VER QUE RESULTADOS DEVUELVO */
  if (isset($setMod)) { /* SI SETEO MODELO */
     if (isset($setTipo)) { /* Y TAMBIEN TIPO */
        if($filtros!='') /* Y TAMBIEN CARACTERISTICAS */
            {
              $consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Vehiculos_Caracteristicas AS vc ON (vc.idVehiculo = ve.idVehiculo) INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE vc.idCaracteristica in ($filtros) GROUP by vc.idVehiculo HAVING count(*)=$cant AND md.Modelo LIKE '$modelo' AND t.tipo Like '$tipo' ";
              
              return $consulta;
              
                           
            }

        else /* SI NO SETEO CARACTERISTICAS PERO SI MODELO Y TIPO */
          {$consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE md.Modelo LIKE '$modelo' AND t.tipo Like '$tipo'";
           return $consulta;
             }
       
       
     }else {/* SI NO SETEO TIPO PERO SI MODELO */
        if($filtros!='') /* SI NO SETEO TIPO PERO SI MODELO Y CARACTERISTICAS */
            {
              $consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Vehiculos_Caracteristicas AS vc ON (vc.idVehiculo = ve.idVehiculo) INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE vc.idCaracteristica in ($filtros) GROUP by vc.idVehiculo HAVING count(*)=$cant AND md.Modelo LIKE '$modelo' ";
              return $consulta;
             }
        else /* SI NO SETEO TIPO NI CARACTERISTICAS PERO SI MODELO */
          {$consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE md.Modelo LIKE '$modelo'";
        
            return $consulta;
          }
        }
  }elseif (isset($setTipo)) {/* SI NO SETEO MODELO PERO SI TIPO */
        if($filtros!='') {/* SI NO SETEO MODELO PERO SI TIPO Y CARACTERISTICAS */
          
            $consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Vehiculos_Caracteristicas AS vc ON (vc.idVehiculo = ve.idVehiculo) INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE vc.idCaracteristica in ($filtros) GROUP by vc.idVehiculo HAVING count(*)=$cant AND t.tipo Like '$tipo' ";
           
              return $consulta;
            }
        else /* SI NO SETEO MODELO NI CARACTERISTICAS PERO SI SETEO EL TIPO  */
        {$consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE t.tipo Like '$tipo'";
         return $consulta;
         }  
     
     
  }else { /* SI NO SETEO MODELO NI TIPO */
     if($filtros!='') /* SI NO SETEO MODELO, NI TIPO PERO SI CARACTERISTICAS */

            {             
              $consulta="SELECT * FROM Vehiculos AS ve INNER JOIN Vehiculos_Caracteristicas AS vc ON (vc.idVehiculo = ve.idVehiculo) INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) WHERE vc.idCaracteristica in ($filtros) GROUP by vc.idVehiculo HAVING count(*)=$cant  ";
               
              return $consulta;

              }else {/* SI NO SETEO NADA, DEVUELVO TODO */
                $consulta = "SELECT * FROM Vehiculos AS ve  INNER JOIN Modelos AS md INNER JOIN Tipos AS t ON (t.idTipo = ve.idTipo) INNER JOIN Marcas AS ma ON (ve.idModelo=md.idModelo AND md.idMarca=ma.idMarca) ORDER BY ma.Marca ASC ";
                return $consulta;
            }
  }
  
 

}
?>