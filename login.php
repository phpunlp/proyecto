<?php
require_once('conexion.php');
require_once("modelUsuario.php");
session_start();
if ((isset($_POST['Usuario'])) && (isset($_POST['Clave']))){
    try{
        $usuario = new Usuario($_POST['Usuario'],$_POST['Clave']);
        $usuario->login();
        $_SESSION['usuario'] = $usuario;
        header('location:backend.php');
    }catch (Exception $e){
        $_SESSION['exception'] = $e;
        header('Location:'.getenv('HTTP_REFERER'));
    }
}
