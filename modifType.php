<?php
    require_once ('cabecera.php');
    require_once('funciones.php');
    checkLogin();
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default active" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <article style="width: 80%;" class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <?php if (empty($_POST)){?>
        <h4 class="text-primary">
            Modificar Tipo de Vehiculo
        </h4>

        <!-- Div donde mostrar error del formulario -->
        <div id="divError"></div>

        <!-- FORMULARIO PARA DAR DE ALTA UN NUEVO TIPO DE VEHICULO (SE REENVIA ASI MISMO EL FORMULARIO-->
        <form class="form-horizontal" action="modifType.php" method="post" onsubmit="return validaModyType()">
            <!-- Select Tipo -->
            <div class="form-group" id="divSelTipo" onchange="setDivFormGroup(this)">
                <label for="selTipo">Seleccione un tipo a modificar:</label>
                <select class="form-control" id="selTipo" name="selectTipo"
                        onchange="ocultaMuestraInput(this,document.getElementById('divNomTipo'));
                        borrarError(document.getElementById('divError'));
                        ">
                    <option selected value="0">Seleccione un Tipo</option>
                    <?php selectTipo(); ?>
                </select>
            </div>

            <div class="form-group" id="divNomTipo" hidden onclick="setDivFormGroup(this)">
                <label for="nomTipo">Nombre Del Tipo:</label>
                <input class="form-control" type="text" id="nomTipo" name="nombreTipo"
                       placeholder="Ingrese un Tipo de Vehiculo" onclick="borrarError(document.getElementById('divError'))">
            </div>
            <input type="submit" class="btn btn-default" value="Enviar">
        </form>
        <?php }else{
            if ((isset($_POST['selectTipo'])) && (isset($_POST['nombreTipo']))){
                modificarTipo($_POST['selectTipo'],strtolower($_POST['nombreTipo']));
            }
        } ?>
    </article>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>