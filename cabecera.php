
<?php
require_once("conexion.php");
require_once('modelUsuario.php');
require_once ("funciones.php");
session_start();



if((isset($_SESSION['usuario'])) && ($_SESSION['usuario']->getLogueado())){
    try{
        $mysqli = conectarBD();
        $user = $_SESSION['usuario'];
        $userEmail = $user->getEmail();
        $query = "SELECT idUsuario FROM usuarios WHERE email = '$userEmail'";
        $result = $mysqli->query($query);
        $id = $result->fetch_assoc();
    }catch (Exception $e){
        errorMessage($e);
    }

    echo '
<nav class="navbar navbar-default text-center" role="navigation">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link rel="icon" type="image/png" href="imagenes/favicon.png" />   
<a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span><strong> Home</strong></a>
<a href="altaVehiculo.php">  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span><strong> Publicar</strong></a> 
<a href="backend.php"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <strong> Backend</strong></a> 
<a href="cerrarSesion.php"> <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span><strong> Cerrar Sesion</strong></a>
</nav>';
}
else{
    echo'<nav class="navbar navbar-default text-center" role="navigation">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">   
<a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span><strong> Home</strong></a>
</nav>';
}
