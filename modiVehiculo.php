<?php
/* Si no se inicio sesion, se redirije a index.php */
require_once ('cabecera.php');
require_once ('funciones.php');
checkLogin();
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar-left">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default active" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <div class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <!-- Si no se envio el formulario, $_GET esta vacio y no esta definido idVehiculo muestra
        El formulario principal, procesa dicho formulario , si esta
        definido muestra el mismo formulario con los datos tomados de la BD, y procesa el formulario de modificacion -->
        <?php if (((empty($_POST)) || (!empty($_GET))) && (!isset($_GET['idVehiculo']))) { ?>
            <h4 class="text-primary">
                Modificar un Vehiculo
            </h4>

            <!--MENSAJES DE ERROR-->
            <div class="alert <?php
            if (isset($_GET['error'])){
                if ($_GET['error'] === true){
                    echo "alert-error";
                }else{
                    echo "alert-success";
                }
            }?>" id="divError">
                <?php if (isset($_GET['error'])=== true){
                    if ($_GET['error'] === true){
                        echo "Ocurrio un error en la actualizacion del vehiculo";
                    }else{
                        echo "Se actualizo correctamente el vehiculo";
                    }
                } ?>
            </div>

                                                        <!-- FORMULARIO bajaVehiculo (SE REENVIA ASI MISMO EL FORMULARIO-->
            <form class="form-horizontal" action="listadoModiVehiculo.php" method="post"
                  onsubmit="return validaBajaMyM()">
                <div class="form-group" id="divSelMarca">
                    <label for="selMarca">Seleccione Marca:</label>
                    <select class="form-control" id="selMarca" name="selectMarca"
                            onchange="location.href='modiVehiculo.php?selectMarca='+this.value">
                        <?php
                        if (empty($_GET['selectMarca'])) { ?>
                            <option selected value="0">Ninguno</option>
                            <?php selectMarca();
                        } else { ?>
                            <option value="0">Ninguno</option>
                            <?php selectedMarca($_GET['selectMarca']); } ?>
                    </select>
                </div>

                <div class="form-group" id="divSelMod">
                    <label for="selMod">Seleccione Modelo:</label>
                    <select class="form-control" id="selMod" name="selectModelo"
                            onchange="setDivFormGroup(document.getElementById('divSelMod'))">
                        <option selected value="0">Ninguno</option>
                        <?php
                        selectModeloSegunidMarca($_GET['selectMarca']);
                        ?>
                    </select>
                </div>

                <!-- HACER CON PHP QUE SE MUESTREN TODOS LOS DATOS DE DICHO VEHICULO -->
                <input type="submit" class="btn btn-default" value="Enviar">
            </form>
            <?php
        }
            /*
             * Si idVehiculo esta definido quiere decir que ya se selecciono en el listado el vehiculo a modificar
             * -> muestra el segundo formulario y procesa el mismo
             */
            if (isset($_GET['idVehiculo'])){ ?>
                <!-- Formulario 2, modificacion vehiculo, mismo formulario que alta vehiculo pero con los datos
                Pre cargados -->
                <h4 class="text-primary">
                    Modificar el Vehiculo
                </h4>

                <!-- DIV DONDE APARECE EL ERROR -->
                <div class="alert <?php
                if (isset($_GET['error'])){
                    if ($_GET['error']===true){
                        echo "alert-danger";
                    }else{
                        echo "alert-success";
                    }
                }?>" id="divError">
                <?php if (isset($_GET['error'])){
                    if ($_GET['error']===true){
                        echo "Error al actualizar el vehiculo";
                    }else{
                        echo "Se modifico correctamente el vehiculo";
                    }
                }?>
                </div>

                                                            <!-- FORMULARIO NUEVA CARACTERISTICA (SE REENVIA ASI MISMO EL FORMULARIO-->
                <form class="form-horizontal" name= "formModVehiculo" action="modificarVehiculo.php?idVehiculo=<?php echo $_GET['idVehiculo'];?>" method="post"
                      enctype="multipart/form-data"  onsubmit="return validaModVehiculo()">

                    <!-- SELECT Tipo de Vehiculo -->
                    <div class="form-group" id="divTipo" onclick="setDivFormGroup(this)">
                        <label for="idTipo"> Seleccione Tipo de vehiculo:</label>
                        <select id="idTipo" name="tipo" class="form-control">
                            <option value="0">Debe seleccionar un Tipo de Vehiculo</option>
                            <?php
                            selectedTipo($_GET['idVehiculo']);
                            ?>
                        </select>
                    </div>

                    <!-- Campo select de modelo -->
                    <div class="form-group" id="divModelo" onclick="setDivFormGroup(this)">
                        <label for="idModelo">Seleccion Modelo</label>
                        <select id="idModelo" name="modelo" class="form-control">
                            <option value="0">Debe seleccionar un modelo de vehiculo</option>
                            <?php
                            selectedModelo(getIdModelo($_GET['idVehiculo']));
                            ?>
                        </select>
                    </div>

                    <!-- Input DOMINIO AUTOMOTOR -->
                    <div class="form-group" id="divDominio" onclick="setDivFormGroup(this)">
                        <label for="idDominio">Dominio Vehiculo:</label>
                        <input type="text" id="idDominio" name="dominio" placeholder="Ingrese el dominio del vehiculo"
                               value=<?php echo "\"".getDominio($_GET['idVehiculo'])."\""; ?> class="form-control">
                    </div>

                    <!-- INPUT AÑO DEL VEHICULO -->
                    <div class="form-group" id="divAnio" onclick="setDivFormGroup(this)">
                        <label for="idAnio">Año del Vehiculo:</label>
                        <input type="text" id="idAnio" <?php echo "value=".getAnio($_GET['idVehiculo']);?> name="anio" placeholder="Ingrese el año del vehiculo"
                               class="form-control">
                    </div>

                    <!-- INPUT PRECIO DEL VEHICULO -->
                    <div class="form-group" id="divPrecio" onclick="setDivFormGroup(this)">
                        <label for="idPrecio">Precio del Vehiculo:</label>
                        <input type="number" id="idPrecio" <?php echo "value=".getPrecio($_GET['idVehiculo']);?> name="precio" placeholder="Ingrese el precio del vehiculo"
                               class="form-control">
                    </div>

                    <!-- Caracteristicas -->
                    <div class="form-control-static" id="divCaracteristicas" onclick="setDivFormGroup(this);
                        borrarError(document.getElementById('divError')">
                        <?php //Carga todos los checkbox dinamicamente
                        getCaracteristicas($_GET['idVehiculo']);
                        ?>
                    </div>

                    <!-- INPUT IMAGEN -->
                    <div class="form-group" id="divImagen" onclick="setDivFormGroup(this)">
                        <label for="idImagen">Agregar Imagen del vehiculo:</label>
                        <input type="file" id="idImagen" name="imagen">
                        <p class="text-left help-block">Formatos admitidos JPEG y JPG</p>
                        <img src="ima.php?id=<?php echo $_GET['idVehiculo']; ?>" style="width: 40%"/>
                    </div>

                    <input type="submit" class="btn btn-default" value="Enviar">
                </form>

        <?php
            }
        ?>
    </div>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>