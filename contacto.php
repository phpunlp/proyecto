<?php
    require_once ('cabecera.php');
    require_once('funciones.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <title>Contacto</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/funciones.js" type="text/javascript"></script>
</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <div class="col-lg-12">
        <div class="col-lg-6">
            <a href="index.php">
                <img src="imagenes/logo.png" alt="75" class="img-rounded"></a>
        </div>

        <!-- FORMULARIO LOGIN -->
        <?php
        if (!isset($_SESSION['usuario'])){
        ?>
        <div class="col-lg-6 left">
            <!-- Div contenedor del formulario de login flotando a derecha del logo -->
            <div class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block">
                <div class="alert " id="divError" style="margin-bottom: inherit;"></div>
                <!-- Formulario de login -->
                <form class="form-inline" action="login.php" method="post" onsubmit="return validaLogin(this)"
                      name="formularioLogin" style="float: right;">
                    <!-- IMPRIME MENSAJES DE ERROR DESDE JS -->
                    <div class="form-group" id="divUsuario">
                        <label for="idUsuario"> Usuario&nbsp;</label>
                        <input type="text" id="idUsuario" name="Usuario" class="form-control"
                               placeholder="Ingrese su Usuario" onclick="borrarError(document.getElementById('divError'));
                               borrarError(document.getElementById('errorLogin'));"/>
                    </div>
                    <div class="form-group" id="divPassword">
                        <label for="password">&nbsp;&nbsp;&nbsp;Clave&nbsp;</label>
                        <input type="password" id="password" name="Clave" class="form-control"
                               placeholder="Ingrese Su Contraseña"
                               onclick="borrarError(document.getElementById('divError'))">
                    </div>

                    <input type="submit" class="btn btn-default" value="Entrar"/>
                </form>
            </div>
            <?php
            }
            if ((isset($_SESSION['exception'])) && $_SESSION['exception']->getMessage() != ""){
                echo "<div id=\"errorLogin\"class=\"alert alert-danger col-lg-7\" style=\"margin-top: 0%;\">"
                    .$_SESSION['exception']->getMessage()."</div>";
                unset($_SESSION['exception']);

            }
            ?>
        </div>
    </div>
</header>

<!-- BARRA DE NAVEGACION -->
<nav class="container-fluid">

    <!-- nav-justified para que tengan todas las opciones li el mismo tamaño
    y ocupen toda la caja que la contiene -->
    <ul class="nav nav-tabs nav-justified">
        <!-- active marca el elemento de la lista como activo -->
        <li><a href="index.php">Principal</a></li>
        <li><a href="info.php">Acerca De:</a></li>
        <li><a href="place.php">Ubicación</a> </li>
        <li><a href="comollegar.php">Como Llegar</a> </li>
        <li class="active"><a href="contacto.php">Contacto</a></li>
    </ul>

</nav>
<!-- CUERPO DE LA PAGINA -->
<section class="panel-body>">

    <?php
    /*
     * Si No se envio el formulario Se muestra el mismo, en caso contrario se valida y envia a enviarMail.php
     * Para su envio
     */
    if (empty($_POST)) {
        ?>
        <article class="container-fluid">
            <h4 class="text-primary">
                Formulario de Contacto:
            </h4>
            <div class="has-error" id="error"></div>

            <!-- Formulario de Contacto -->
            <form action="contacto.php" method="post" name="contacto" onsubmit="return validaContact()">

                <div class="form-group" id="divNom" onclick="setDivFormGroup(this)">
                    <label for="nombre">Nombre</label>
                    <input class="form-control" type="text" name="nomContact" id="nombre"
                           placeholder="Introduce tu nombre">
                </div>

                <div class="form-group" id="divApe" onclick="setDivFormGroup(this)">
                    <label for="apellido">Apellido</label>
                    <input type="text" id="apellido" name="apeContact" class="form-control"
                           placeholder="Introduce tu Apellido">
                </div>

                <div class="form-group" id="divEmail" onclick="setDivFormGroup(this)">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" placeholder="Ingrese Su Correo Electronico" id="email"
                           name="emailContact">
                </div>

                <div class="form-group" id="divAsunto" onclick="setDivFormGroup(this)">
                    <label for="idAsunto">Ingrese Un Asunto</label>
                    <input id="idAsunto" name="asuntoContact" class="form-control"
                    placeholder="Ingrese Un Asunto para la consulta">
                </div>

                <div class="form-group" id="divConsulta" onclick="setDivFormGroup(this)">
                    <label for="consulta">Consulta</label>
                    <textarea class="form-control" id="consulta" rows="4"
                              placeholder="Escriba aquí su consulta" name="consultaContact"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Enviar Formulario">
                </div>

            </form>
        </article>
        <?php
    }else { //Si se envio el formulario valida los camposm, si son correctos envia el email, si no imprime error
        if ((validaText($_POST['nomContact'])) || (validaText($_POST['apeContact'])) ||
            validaEmail($_POST['emailContact']) || validaText($_POST['consultaContact']) ||
            validaText($_POST['asuntoContact'])
        ) {

            $remitente = $_POST['emailContact'];
            $asunto = $_POST['asuntoContact'];
            $nombre = $_POST['nomContact'];
            $apellido = $_POST['apeContact'];
            $consulta = $_POST['consultaContact'];
            enviarEmail($nombre, $apellido, $remitente, $asunto, $consulta);
        }else{
            errorMessage("Error en el formulario, vuelva atras y verifique el mismo.");
        }
    }
    ?>
</section>
<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>