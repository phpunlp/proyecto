<?php
require_once ('conexion.php');
/** Si no se encuentra logueado redirije a index */
function checkLogin(){
    if (!isset($_SESSION['usuario'])){
        header('location:index.php');
    }
}


/**
 * Valida que el nombre pasado por parametro no sea vacio o sean numeros.
 * @param $text
 * @return bool
 */
function validaText($text){
    if ((trim($text) != "") && (!is_numeric($text))){
        return true;
    }
    return false;
}
/**
 * Valida que el mail pasado por parametro no sea vacio o tenga un formato incorrecto
 * @param $mail
 * @return bool
 */
function validaEmail($mail){
    if ((trim($mail) != "") && filter_var($mail,FILTER_VALIDATE_EMAIL)){
        return true;
    }
    return false;
}

/**
 * Retorna el tipo de imagen en caso de ser JPEG o JPG caso contrario retorna false
 * @param $img
 * @return bool|string
 */
function validoTipoImagen($img){
    /*EXIF_IMAGETYPE RETORNA UN NUMERO EQUIVALENTE AL TIPO DE LA IMAGEN*/
    $typefile = $img['type'];
    /*Si el tipo de archivo es JPEG o PNG lo guardo junto al tipo de archivo */
    if (($typefile === "image/jpg") || ($typefile === "image/jpeg") || $typefile === "image/png") {
        return true;
    }
        errorMessage("Tipo de Imagen invalido, Por favor ingrese una imagen en formato JPG, JPEG o PNG");
        return false;
}

/**
 * Imprime en un div de Accion Correcta el mensaje pasado por parametro
 * @param $message
 */
function successMessage($message){
    echo '<div class="alert alert-success">'.$message .'</div>';
}

/**
 * Imprime en un div de Error el mensaje pasado por parametro
 * @param $message
 */
function errorMessage($message){
    echo "<div class=\"alert alert-danger\">$message</div>";
}

/**
 * Imprime Mensaje de alerta
 * @param $message
 */
function alertMessage($message){
    echo "<div class=\"alert alert-warning\">$message</div>";
}
/**
 * Envia un email a la direccion web Que figura en la funcion con los datos pasados por parametro
 * @param $nombre
 * @param $apellido
 * @param $remitente
 * @param $asunto
 * @param $consulta
 */
function enviarEmail($nombre, $apellido, $remitente, $asunto, $consulta){
    $destino = "simchgab@gmail.com";
    $encabezado = "From: $nombre $apellido\nReply-To:$remitente\nContent-Type:text/html; charset=iso-8859-1";
    if (mail($destino,$asunto,$consulta,$encabezado)){
        successMessage("El correo fue enviado con exito");
    }else{
        errorMessage("El correo no pudo ser enviado. Disculpe las molestias");
    }
}

/**
 * Conecta a la BD y retorna $mysqli para realizar consultas
 * @return mysqli
 */
function conectarBD(){
	try{
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_BD);
		if ($mysqli->connect_errno){
        throw new Exception("Error al conectar con la BD N°: ".$mysqli->connect_errno." ".$mysqli->connect_error);
    }else{
        return $mysqli;
    }
	}catch (Exception $e){
		$_SESSION['exception'] = $e;
	}
}

/**
 * Retorna el nombre del modelo con idModelo
 * @param $idModelo
 * @return mixed
 */
function getModelo($idModelo){
            $mysqli = conectarBD();
            $consulta = "SELECT * FROM Modelos WHERE idModelo = '$idModelo'";
            $arreglo = $mysqli->query($consulta, MYSQLI_ASSOC);
            $res = $arreglo->fetch_row();
            $arreglo->free_result();
            $mysqli->close();
            return $res[2];
 }

/**
 * Retorna el idMarca de la marca asociada al modelo idModelo
 * @param $idModelo
 * @return mixed
 */
function getIdMarca($idModelo){
            $mysqli = conectarBD();            
            $consulta = "SELECT * FROM Modelos WHERE idModelo = '$idModelo'";
            $arreglo = $mysqli->query($consulta, MYSQLI_ASSOC);
            $res = $arreglo->fetch_row();
            $arreglo->free_result();
            $mysqli->close();
            return $res[1];
 }

/**
 * Retorna el nombre de la marca con idModelo
 * @param $idModelo
 * @return mixed
 */
function getMarca($idModelo){
            $mysqli = conectarBD();  
            $idMarca = getIdMarca($idModelo);
            $consulta = "SELECT * FROM Marcas WHERE idMarca = '$idMarca'";
            $arreglo = $mysqli->query($consulta, MYSQLI_ASSOC);
            $res = $arreglo->fetch_row();
            $arreglo->free_result();
            $mysqli->close();
            return $res[1];

 }

/**
 *Carga las opciones del select para Seleccion de marcas, junto a su value con el id de cada marca
 */
function selectMarca(){
     $mysqli = conectarBD();
     $query = "SELECT * FROM Marcas";
    if (($mysqli->query($query)->num_rows) > 0) {
        $result = $mysqli->query($query, MYSQLI_ASSOC);
        while ($row = $result->fetch_assoc()) {
            echo "<option  value='" . $row['idMarca'] . "'>" . ucwords($row['Marca']) . "</option>";
        }
        $result->free_result();
        $mysqli->close();
    }
 }

 function selectedMarca($idMarca){
     $query = "SELECT * FROM Marcas";
     $mysqli = conectarBD();
     if (($mysqli->query($query)->num_rows) > 0) {
         $result = $mysqli->query($query);
         while ($row = $result->fetch_assoc()) {
             if ($row['idMarca'] != $idMarca) {
                 echo "<option value =\"" . $row['idMarca'] . "\">" . ucwords($row['Marca']) . "</option>";
             } else {
                 echo "<option selected value=\"" . $row['idMarca'] . "\">" . ucwords($row['Marca']) . "</option>";
             }
         }
         $result->free_result();
         $mysqli->close();
     }
 }
 /**
 * Retorna el nombre del Tipo idTipo
 * @param $idTipo
 * @return string
 */
function getTipo($idTipo){
     $query = "SELECT Tipo FROM Tipos WHERE idTipo='$idTipo'";
     $mysqli = conectarBD();
     $result = $mysqli->query($query)->fetch_row();
     return ucwords($result[0]);
 }
/**
 * Carga dinamicamente las opciones de los select de modelos.
 */
function selectModelo(){
     $mysqli = conectarBD();
     $query = "SELECT * FROM Modelos";
    if ($mysqli->query($query)->num_rows > 0) {
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()) {
            echo "<option value=\"" . $row['idModelo'] . "\">" . ucwords($row['Modelo']) . "</option>";
        }
        $result->free_result();
        $mysqli->close();
    }
 }



 function selectTipo(){
     $mysqli = conectarBD();
     $query = "SELECT * FROM Tipos";
     if ($mysqli->query($query)->num_rows > 0) {
         $result = $mysqli->query($query);
         while ($row = $result->fetch_assoc()) {
             echo "<option  value=\"" . $row['idTipo'] . "\">" . ucwords($row['Tipo']) . "</option>";
         }
         $result->free_result();
         $mysqli->close();
     }
 }

/**
 *Carga las opciones del select de caracteristicas
 */
function selectCaracteristicas(){
     $query = "SELECT * FROM Caracteristicas";
     $mysqli=conectarBD();
    if ($mysqli->query($query)->num_rows > 0) {
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()) {
            echo "<option value=\"" . $row['idCaracteristica'] . "\">" . ucwords($row['Caracteristica']) . "</option>";
        }
        $result->free_result();
        $mysqli->close();
    }
 }

/**
 * Carga la tabla de bajaVehiculo con los vehiculos de la marca y modelo seleccionados previamente
 * @param $idModelo
 * @param $idMarca
 */
function cargaTablaBajaVehiculo($idModelo,$idMarca){
    $query = "SELECT * FROM Vehiculos WHERE idModelo = '$idModelo'";
    $marca = getNomMarca($idMarca);
    $mysqli = conectarBD();
    if ($mysqli->query($query)->num_rows === 0){
            alertMessage("No existen vehiculos para el modelo y marca seleccionados, por favor seleccione otro");
    }else{
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()){
            echo "<tr>";
            echo "<td>".getModelo($idModelo)."</td>";
            echo "<td>".$marca."</td>";
            echo "<td>".$row['anio']."</td>";
            echo "<td>".$row['precio']."</td>";
            echo "<td>".getTipo($row['idTipo'])."</td>";
            echo "<td>".strtoupper($row['dominio'])."</td>";
            echo "<td><a class=\"btn btn-primary\" onclick=\"cartelConfirmacion('eliminarVehiculo.php?idVehiculo=".$row['idVehiculo']."')\">
        Eliminar Vehiculo</a></td>";
            echo "</tr>";
        }
        $result->free_result();
        $mysqli->close();
    }
}
/**
 * Carga el modelo asociado a la marca con idMarca
 * @param $modelo
 * @param $idMarca
 */
function altaModelo($modelo, $idMarca){
    $mysqli = conectarBD();
    $query = "SELECT * FROM Modelos WHERE Modelo = '$modelo'";
    $result = $mysqli->query($query);
    if ($result->num_rows === 1){
        errorMessage("Ya existe el modelo, Ingrese otro gracias.");
    }else{
        $query = "INSERT INTO Modelos (idMarca,Modelo) VALUES ('$idMarca','$modelo')";
        if ($mysqli->query($query) === true){
            successMessage("Se agrego el modelo correctamente");
        }else{
            errorMessage("Error: ".$query."<br>".$mysqli->connect_error);
        }
    }
    $result->free_result();
    $mysqli->close();
}


/**
 * DA DE ALTA LA MARCA PASADA POR PARAMETRO
 * @param $marca
 * @return int|mixed
 */
function altaMarca($marca){
    $mysqli = conectarBD();
    $query = "SELECT * FROM Marcas WHERE Marca='$marca'";
    $result = $mysqli->query($query);
    $idNuevaMarca = 0;
    if ($result->num_rows === 1){ //Ya existe la marca
        errorMessage("Ya existe la marca, por favor seleccionela del menu desplegable de arriba");
    }else{
        $query = "INSERT INTO Marcas (marca) VALUE ('$marca')";
        if ($mysqli ->query($query) === true){
            successMessage("La nueva marca fue guardada correctamente");
            $idNuevaMarca = $mysqli->insert_id;
        }else{
            errorMessage("Error: ".$query."<br>".$mysqli->connect_error);
        }
    }
    $result->free_result();
    $mysqli->close();
    return $idNuevaMarca;
}

/**
 * SI NO HAY VEHICULOS ASOCIADOS -> SI NO HAY MAS DE 1 MODELO ASOCIADO A LA MARCA ELIMINA MARCA Y MODELO, SI NO
 * ELIMINA SOLO EL MODELO
 * @param $idMarca
 * @param $idModelo
 */
function bajaMarcaModelo($idMarca, $idModelo){
    $query = "DELETE FROM Modelos WHERE idModelo = '$idModelo'";
    $consulta = "SELECT * FROM Modelos WHERE idMarca='$idMarca'";
    $consultaVehiculo = "SELECT * FROM Vehiculos WHERE idModelo='$idModelo'";
    $query2 = "DELETE FROM Marcas WHERE idMarca = '$idMarca'";
    $mysqli = conectarBD();
    //Si hay mas de 1 modelo asociado a la marca a borrar, y no existen vehiculos asociados al modelo solo se borra el modelo
    if (($mysqli->query($consulta)->num_rows > 1) && ($mysqli->query($consultaVehiculo)->num_rows === 0)){
        if ($mysqli->query($query) === true){
            alertMessage("Se elimino correctamente el modelo, no asi la marca ya que se perderian las referencias 
            a otros modelos de la marca, primero elimine todos los modelos pertenecientes a la marca");
        }else{
            errorMessage("Error:".$query."</br>".$mysqli->connect_error);
        }
    }else{
        if ($mysqli->query($consultaVehiculo)->num_rows > 0){ //Si hay vehiculos asociados al modelo error
            errorMessage("No se puede eliminar el modelo, dado que hay vehiculos de este modelo asociados,
             Primero elimine dichos vehiculos");
        }else if ($mysqli->query($query) === true){ //Si no borro marca y modelo
            if ($mysqli->query($query2) === true){
                successMessage("Se elimino el modelo y la marca seleccionada");
            }else{
                errorMessage("Error: ".$query2."</br>".$mysqli->connect_error);
            }
        }else{
            errorMessage("Error: ".$query."</br>".$mysqli->connect_error);
        }
    }
    $mysqli->close();
}

/**
 * Se modifica la marca con idMarca
 * Se verifica en principio que el nuevo nombre no exista ya en la BD, si existe se reporta un error
 * Si no existe el nombre, se verifica la existencia del id de marca, de existir se procede a la modificacion
 * @param $marca
 * @param $idMarca
 */
function modificarMarca($marca, $idMarca){
    $query = "SELECT * FROM Marcas WHERE idMarca='$idMarca'";
	$consultanombre = "SELECT * FROM Marcas WHERE Marca = '$marca'";
    $modificar = "UPDATE Marcas SET Marca='$marca' WHERE idMarca='$idMarca'";
    $mysqli = conectarBD();
	if ($mysqli->query($consultanombre)->num_rows > 0){
		errorMessage("Error: El nuevo nombre para esta marca ya existe en la BD");
	}else{
     if ($mysqli->query($query)->num_rows > 0){
         if ($mysqli->query($modificar) === true){
             successMessage("Se modifico la marca correctamente");
         }else{
             errorMessage("Error: ".$modificar."</br>".$mysqli->connect_error);
         }
         $mysqli->close();
     }else{
         if ($idMarca > 0) {
             errorMessage("No existe la marca");
         }
     }
    }
}

/**
 * Se modifica el modelo con idModelo
 * Se verifica en principio que el nuevo nombre no exista ya en la BD, si existe se reporta un error
 * Si no existe el nombre, se verifica la existencia del id de marca, de existir se procede a la modificacion
 * @param $modelo
 * @param $idModelo
 */
function modificarModelo($modelo, $idModelo){
    $query = "SELECT * FROM Modelos WHERE idModelo='$idModelo'";
	$consultaNombre = "SELECT * FROM Modelos WHERE Modelo='$modelo'";
    $modificar = "UPDATE Modelos SET Modelo='$modelo' WHERE idModelo='$idModelo'";
    $mysqli = conectarBD();
	if ($mysqli->query($consultaNombre)->num_rows > 0){
		errorMessage("Error: El nuevo nombre para el modelo ya existe en la BD");
	}else{
     if ($mysqli->query($query)->num_rows > 0){
         if ($mysqli->query($modificar) === true){
             successMessage("Se modifico correctamente el modelo");
         }else{
             errorMessage("Error: ".$modificar."</br>".$mysqli->connect_error);
         }
         $mysqli->close();
     }else{
         if ($idModelo >0) {
             errorMessage("No existe el modelo");
         }
     } 
    }
}

/**
 * Si la caracteristica pasada por parametro ya existe en la BD, imprime un error
 * Caso contrario la agrega
 * @param $caracteristica
 */
function altaCaracteristica($caracteristica){
    $mysqli = conectarBD();
    $query = "SELECT idCaracteristica FROM Caracteristicas WHERE Caracteristica = '$caracteristica'";
    $result = $mysqli -> query($query);
    if ($result->num_rows === 1){
        errorMessage("Ya existe la caracteristica, por favor ingrese una distinta");
        $result->free_result();
    }else{
        $query = "INSERT INTO Caracteristicas (Caracteristica) VALUES ('$caracteristica')";
        if ($mysqli->query($query) === true) {
            successMessage("Se Guardo correctamente la nueva Caracteristica");
        }else{
            errorMessage("Error: ".$query."<br>".$mysqli->connect_error);
        }
    }
    $mysqli->close();
}

/**
 * Si no hay elementos asociados a la caracteristica pasada por parametro la elimina, si no informa error
 * @param $idCaracteristica
 */
function bajaCaracteristica($idCaracteristica){
    $consultaVehiculoCaracteristicas = "SELECT * FROM Vehiculos_Caracteristicas WHERE idCaracteristica='$idCaracteristica'";
    $query = "DELETE FROM Caracteristicas WHERE idCaracteristica='$idCaracteristica'";
    $mysqli = conectarBD();
    if ($mysqli->query($consultaVehiculoCaracteristicas)->num_rows > 0){
        errorMessage("No se puede eliminar la caracteristica seleccionada dado que hay vehiculos, asociados a dicha
         caracteristica, primero elimine dichos vehiculos");
    }else if ($mysqli->query($query) === true){
        successMessage("La caracteristica seleccionada fue eliminada correctamente");
    }else{
        errorMessage("Error: ".$query."</br>".$mysqli->connect_error);
    }
}

/**
 * Modifica la caracteristica con idCaracteristica con el nombre pasado por parametro
 * @param $idCaracteristica
 * @param $nombre
 */
function modificarCaracteristica($idCaracteristica, $nombre){
    $query = "SELECT * FROM Caracteristicas WHERE idCaracteristica='$idCaracteristica'";
	$consultaNombre= "Select * FROM Caracteristicas WHERE Caracteristica='$nombre'";
    $modificar = "UPDATE Caracteristicas SET Caracteristica='$nombre' WHERE idCaracteristica='$idCaracteristica'";
    $mysqli = conectarBD();
	if ($mysqli->query($consultaNombre)->num_rows > 0){
		errorMessage("Error: El nuevo nombre para la caracteristica ya existe en la BD");
	}else{
        if ($mysqli->query($query)->num_rows > 0){
           if ($mysqli->query($modificar) === true){
           successMessage("Se modifico la categoria correctamente");
        }else{
           errorMessage("Error: ".$modificar."</br>".$mysqli->connect_error);
        }
        $mysqli->close();
        }else {
         errorMessage("No hay ninguna categoria que concuerde con la seleccionada");
        }
    }
}

/**
 * Se comprueba que el tipo pasado por parametro no exista en la BD, si existe imprime error, si no lo da de alta
 * @param $tipo
 */
function altaTipo($tipo){
    $query = "SELECT idTipo FROM Tipos WHERE Tipo='$tipo'";
    $mysqli = conectarBD();
    if ($mysqli->query($query)->num_rows === 1){ //Si el tipo ya existe
        errorMessage("Ya existe el tipo de vehiculo, por favor ingrese uno nuevo");
    }else{
        $query = "INSERT INTO Tipos (Tipo) VALUE ('$tipo')";
        if ($mysqli->query($query) === true){
            successMessage("Se agrego correctamente el tipo de vehiculo");
        }else{
            errorMessage("Error: ".$query."<br>".$mysqli->connect_error);
        }
    }
    $mysqli->close();
}

/**
 * Elimina el tipo de vehiculo seleccionado, si no hay ningun vehiculo asociado, si no muestra el error
 * @param $idTipo
 */
function eliminarTipo($idTipo){
    $query = "DELETE FROM Tipos WHERE idTipo = '$idTipo'";
    $mysqli = conectarBD();
    if ($mysqli->query($query) === true){
        successMessage("Se borro correctamente el Tipo seleccionado");
    }else{
        errorMessage("Es imposible eliminar el tipo: ".$query."</br>".$mysqli->error);
    }
}

/**
 * Modifica el tipo con $idTipo con el $nombreTipo
 * @param $idTIpo
 * @param $nombreTipo
 */
function modificarTipo($idTIpo, $nombreTipo){
    $consulta = "SELECT * FROM Tipos WHERE idTipo='$idTIpo'";
	$consultaNombre="SELECT * FROM Tipos WHERE Tipo='$nombreTipo'";
    $query = "UPDATE Tipos SET Tipo='$nombreTipo' WHERE idTipo='$idTIpo'";
    $mysqli = conectarBD();
	if ($mysqli->query($consultaNombre)->num_rows > 0){
		errorMessage("Error: El nuevo nombre para el tipo de vehiculo ya existe en la BD");
	}else{
     if ($mysqli->query($consulta)->num_rows > 0){
         if ($mysqli->query($query) ===  true){
             successMessage("El tipo fue modificado correctamente");
         }else{
             errorMessage("Error: ".$query."</br>".$mysqli->connect_error);
         }
     }else{
         alertMessage("No se encontro ningun tipo con ese nombre");
     }
 }
}

/**
 * DA DE ALTA EL VEHICULO
 * @param $modelo
 * @param $tipo
 * @param $dominio
 * @param $anio
 * @param $precio
 * @param $caracteristicas
 * @param $foto
 */
function altaVehiculo($modelo, $tipo, $dominio, $anio, $precio, $caracteristicas, $foto)
{
    $binarioImagen = obtenerBinarioIMG($foto); //Extraigo el binario para guardar en la BD
    $tipoImagen = retornaTipoImagen($foto);
    $query = "SELECT dominio FROM Vehiculos WHERE dominio='$dominio'";
    $mysqli = conectarBD();
    if ($mysqli->query($query)->num_rows > 0) {
        errorMessage("Ya existe el vehiculo, por favor ingrese uno nuevo");
    } else {
        $query = "INSERT INTO Vehiculos (idModelo, idTipo, dominio, anio, precio, contenidoimagen, tipoimagen) VALUES 
              ('$modelo','$tipo','$dominio','$anio','$precio','$binarioImagen','$tipoImagen')";
        if ($mysqli->query($query) === true) {
            if (!empty($caracteristicas)) {
                $idVehiculo = $mysqli->insert_id;
                //Si se pudo referenciar las caracteristicas correctamente al vehiculo
                referenciarVehiculoCaracteristica($idVehiculo, $caracteristicas);
                successMessage("se dio de alta correctamente el vehiculo junto con sus caracteristicas");
            } else {
                successMessage("Se dio de alta el vehiculo correctamente sin caracteristicas");
            }
        }else{
            errorMessage("No se pudo dar de alta el vehiculo");
        }
    }
}

/**
 * Carga la tabla de modificacion de vehiculo
 * @param $idModelo
 * @param $idMarca
 */
function cargaTablaModiVehiculo($idModelo, $idMarca){
    $query = "SELECT * FROM Vehiculos WHERE idModelo = '$idModelo'";
    $marca = getNomMarca($idMarca);
    $mysqli = conectarBD();
    if ($mysqli->query($query)->num_rows === 0){
        alertMessage("No existen vehiculos para el modelo y marca seleccionados, por favor seleccione otro");
    }else{
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()){
            echo "<tr>";
            echo "<td>".getModelo($idModelo)."</td>";
            echo "<td>".$marca."</td>";
            echo "<td>".$row['anio']."</td>";
            echo "<td>".$row['precio']."</td>";
            echo "<td>".getTipo($row['idTipo'])."</td>";
            echo "<td>".strtoupper($row['dominio'])."</td>";
            echo "<td><a class=\"btn btn-primary\" onclick=\"cartelConfirmacion('modiVehiculo.php?idVehiculo=".$row['idVehiculo']."')\">
        Modificar Vehiculo</a></td>";
            echo "</tr>";
        }
        $result->free_result();
        $mysqli->close();
    }
}


/**
 * Carga en la tabla de vehiculos_caracteristicas las caracteristicas del vehiculo agregado recientemente
 * @param $idVehiculo
 * @param $caracteristicas
 */
function referenciarVehiculoCaracteristica($idVehiculo, $caracteristicas){
    $mysqli = conectarBD();
    //Cargo las caracteristicas seleccionadas en la tabla vehiculos_caracteristicas con idVehiculo y el id de la caract
    foreach ($caracteristicas as $caracteristica) {
        $query = "INSERT INTO Vehiculos_Caracteristicas (idVehiculo, idCaracteristica) VALUES ('$idVehiculo','$caracteristica')";
        if ($mysqli->query($query) === false) {
            errorMessage("Error al agregar la caracteristica al vehiculo: ".$query."</br>".$mysqli->connect_error);
        }
    }
    $mysqli->close();
}

/**
 * Extrae el binario de una imagen para poder ser guardado en el blob de la base de datos
 * @param $foto
 * @return string
 */
function obtenerBinarioIMG($foto){
    $imagen = $foto['tmp_name']; //Guardo la imagen en una variable
    $binario_contenido = addslashes(fread(fopen($imagen, "rb"), filesize($imagen))); //Lee el archivo para guardarlo en la BD
    return $binario_contenido;
}

/**
 * Retorna un string indicando el tipo de imagen correspondiente jpeg, jpg o png
 * @param $foto
 * @return string
 */
function retornaTipoImagen($foto){
    $tipoImagen = $foto['type'];
    if ($tipoImagen === "image/jpeg"){
        return "jpeg";
    }else if ($tipoImagen === "image/jpg"){
        return "jpg";
    }
    return "png"; //Al ya estar validada la imagen, si no es de uno de los 2 formatos anteriores si o si es png
}
/**
 * Carga dinamicamente la lista de caracteristicas
 */
function cargaCaracteristicasHTML(){
    $query = "SELECT * FROM Caracteristicas";
    $mysqli = conectarBD();
    if ($mysqli->query($query)->num_rows === 0){ //No hay caracteristicas en la BD
        errorMessage("No existen caracteristicas, por favor de de alta una antes");
    }else{
        $result = $mysqli->query($query,MYSQLI_ASSOC);
        while ($row = $result->fetch_array()){
            echo "<input type=\"checkbox\" value=\"".$row['idCaracteristica']."\" id=\"idCaract"
                .$row['idCaracteristica']."\" name=\"caracteristicaVehiculo[]\" class=\"checkbox-inline\"/>";
            echo " ";
            echo "<label for=\"idCaract".$row['idCaracteristica']."\">".$row['Caracteristica']."</label>";
            echo " ";
              }
        $result->free_result();
    }
    $mysqli->close();
}

/**
 * Retorna el nombre de la marca pasada por parametro (ID)
 * @param $idMarca
 * @return bool|mysqli_result
 */
function getNomMarca($idMarca){
    $query = "SELECT Marca FROM Marcas WHERE idMarca='$idMarca'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_row();
    $nom = ucwords($result[0]);
    return $nom;
}

/**
 * Retorna el dominio del vehiculo pasado por parametro
 * @param $idVehiculo
 * @return string
 */
function getDominio($idVehiculo){
    $query = "SELECT dominio FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_row();
    $nom = ucwords($result[0]);
    $mysqli->close();
    return strtoupper($nom);
}

/**
 * Retorna el año del vehiculo con idVehiculo recibido
 * @param $idVehiculo
 * @return mixed
 */
function getAnio($idVehiculo){
    $query = "SELECT anio FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_row();
    $anio = $result[0];
    $mysqli->close();
    return $anio;
}
/**
 * Carga el select de tipo, con selectValue en el tipo correspondiente al idVehiculo recibido por parametro
 * @param $idVehiculo
 */
function selectedTipo($idVehiculo){
    $query = "SELECT idTipo FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $queryTipos = "SELECT * FROM Tipos";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_row();
    $idTipo = $result[0];
    $result = $mysqli->query($queryTipos);
            while ($row = $result->fetch_assoc()) {
                if ($row['idTipo'] != $idTipo) {
                    echo "<option value =\"" . $row['idTipo'] . "\">" . ucwords($row['Tipo']) . "</option>";
                } else {
                    echo "<option selected value=\"" . $row['idTipo'] . "\">" . ucwords($row['Tipo']) . "</option>";
                }
            }
            $result->free_result();
            $mysqli->close();
}


/**
 * Retorna el idModelo a partir del idVehiculo recibido
 * @param $idVehiculo
 * @return mixed
 */
function getIdModelo($idVehiculo){
    $query = "SELECT idModelo FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_array();
    $idModelo = $result[0];
    $mysqli->close();
    return $idModelo;
}

/**
 * Carga todos los modelos en el select, pero el modelo pasado por parametro lo coloca como selected
 * @param $idModelo
 */
function selectedModelo($idModelo){
    $query = "SELECT * FROM Modelos";
    $mysqli = conectarBD();
    $result = $mysqli->query($query);
    while ($row = $result->fetch_assoc()) {
        if ($row['idModelo'] != $idModelo) {
            echo "<option value =\"" . $row['idModelo'] . "\">" . ucwords($row['Modelo']) . "</option>";
        } else {
            echo "<option selected value=\"" . $row['idModelo'] . "\">" . ucwords($row['Modelo']) . "</option>";
        }
    }
    $result->free_result();
    $mysqli->close();
}

/**
 * Retorna el precio del vehiculo idVehiculo
 * @param $idVehiculo
 * @return mixed
 */
function getPrecio($idVehiculo){
    $query = "SELECT precio FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_row();
    $precio = $result[0];
    $mysqli->close();
    return $precio;
}

/**
 * Carga ya checkeados los checkbox leidos de la base de datos para el idVehiculo, el resto de los checkbox existentes
 * Los carga sin checkear.
 * @param $idVehiculo
 */
function getCaracteristicas($idVehiculo){
    //Consulta que devuelve todos los idCaracteristicas correspondientes al vehiculo
    $query = "SELECT idCaracteristica FROM Vehiculos_Caracteristicas WHERE idVehiculo='$idVehiculo'";
    $caracteristicasTot = "SELECT * FROM Caracteristicas"; //junta todas las caracteristicas
    $mysqli = conectarBD();
    //Si hay caracteristicas para dicho vehiculo
        $todasCaract = $mysqli->query($caracteristicasTot);
        //almaceno en result todos los idCaracteristica correspondientes a dicho idVehiculo
        $result = $mysqli->query($query);
        $row = $result->fetch_array();
        $idCaracteristica = $row[0];
        while ($caracteristica = $todasCaract->fetch_assoc()){
                if ($caracteristica['idCaracteristica'] === $idCaracteristica) {
                    echo "<input type=\"checkbox\" value=\"" . $idCaracteristica . "\" id=\"idCaract" . $idCaracteristica . "\" name=\"caracteristicaVehiculo[]\"
                    checked class =\"checkbox-inline\"/>";
                    echo " ";
                    echo "<label for=\"idCaract" . $idCaracteristica . "\">" . $caracteristica['Caracteristica'] . "</label>";
                    echo "  ";
                    if (!empty($result)) {
                        $row = $result->fetch_array();
                        $idCaracteristica = $row[0];
                    }
                }else{
                    echo "<input type=\"checkbox\" value=\"".$caracteristica['idCaracteristica']."\" id=\"idCaract".$caracteristica['idCaracteristica']."\" name=\"caracteristicaVehiculo[]\"
            class =\"checkbox-inline\"/>";
                    echo " ";
                    echo "<label for=\"idCaract".$caracteristica['idCaracteristica']."\">".ucwords($caracteristica['Caracteristica'])."</label>";
                    echo " ";
            }

        }
        $result->free_result();    $mysqli->close();
}

function impCaracteristicas($idVehiculo){
    //Consulta que devuelve todos los idCaracteristicas correspondientes al vehiculo
    $query = "SELECT idCaracteristica FROM Vehiculos_Caracteristicas WHERE idVehiculo='$idVehiculo'";
    $caracteristicasTot = "SELECT * FROM Caracteristicas"; //junta todas las caracteristicas
    $mysqli = conectarBD();
    //Si hay caracteristicas para dicho vehiculo
        $todasCaract = $mysqli->query($caracteristicasTot);
        //almaceno en result todos los idCaracteristica correspondientes a dicho idVehiculo
        $result = $mysqli->query($query);
        $row = $result->fetch_array();
        $idCaracteristica = $row[0];
        if (($result->num_rows)>0) {
           
       
        while ($caracteristica = $todasCaract->fetch_assoc()){
                if ($caracteristica['idCaracteristica'] === $idCaracteristica) {
                    echo "<input" . $idCaracteristica . "\" id=\"idCaract" . $idCaracteristica . "\" name=\"caracteristicaVehiculo[]\"
                    checked class =\"checkbox-inline\"/>";
                    echo " ";
                    echo "<label for=\"idCaract" . $idCaracteristica . "\">" . ucwords($caracteristica['Caracteristica']) . "</label>";
                    echo "  ";
                    if (!empty($result)) {
                        $row = $result->fetch_array();
                        $idCaracteristica = $row[0];
                    }
                }
 
           }
         }else echo "Consulte las caracteristicas al vendedor";
        $result->free_result();    $mysqli->close();
}

/**
 * Elimina todas las caracteristicas asociadas al vehiculo, y vuelve a cargar las nuevas modificaciones.
 * @param $caracteristicas
 * @param $idVehiculo
 */
function modificarCaracteristicasVehiculo($caracteristicas, $idVehiculo){
    $borraCaracteristicas = "DELETE FROM Vehiculos_Caracteristicas WHERE idVehiculo=$idVehiculo";
    $mysqli = conectarBD();
    if ($mysqli->query($borraCaracteristicas) === true){
        $mysqli->close();
        referenciarVehiculoCaracteristica($idVehiculo,$caracteristicas);
    }else{
        errorMessage("Error:".$borraCaracteristicas." ".$mysqli->error);
    }
}
/**
 * Modifica el vehiculo, sin modificar la imagen del mismo
 * @param $idVehiculo
 * @param $tipo
 * @param $modelo
 * @param $dominio
 * @param $anio
 * @param $precio
 */
function updateSinImagen($idVehiculo, $tipo, $modelo, $dominio, $anio, $precio,$caracteristicas){
    //Modifica los datos del vehiculo con idVehiculo, sin modificar la imagen del mismo
    $query = "UPDATE Vehiculos SET idModelo='$modelo',idTipo='$tipo',dominio='$dominio',anio='$anio',precio='$precio' WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    if ($mysqli->query($query) === true){
        $mysqli->close();
        modificarCaracteristicasVehiculo($caracteristicas, $idVehiculo);
            header('location:modiVehiculo.php?error='.false);
    }else{
        $mysqli->close();
        header('location:modiVehiculo.php?error='.true);
    }

}

/**
 * Actualiza el auto con idVehiculo junto a la imagen añadida
 * @param $idVehiculo
 * @param $tipo
 * @param $modelo
 * @param $dominio
 * @param $anio
 * @param $precio
 * @param $binario_imagen
 * @param $tipoImagen
 */
function updateConImagen($idVehiculo, $tipo, $modelo, $dominio, $anio, $precio, $binario_imagen, $tipoImagen,$caracteristicas){
    $query = "UPDATE Vehiculos SET idModelo='$modelo',idTipo='$tipo',dominio='$dominio',anio='$anio',precio='$precio',
contenidoimagen='$binario_imagen',tipoimagen='$tipoImagen' WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    if ($mysqli->query($query)=== true){
        $mysqli->close();
        modificarCaracteristicasVehiculo($caracteristicas, $idVehiculo);
        header('location:modiVehiculo.php?error='.false);
    }else{
        $mysqli->close();
        header('location:modiVehiculo.php?error='.true);
    }

}

function datosUsuario($idUsuario){
    $query = "SELECT * FROM usuarios WHERE idUsuario='$idUsuario'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query);
    while ($row = $result->fetch_assoc()){
        echo "<span class='jumbotron'> Bienvenido Nuevamente ".$row['nombre']." ".$row['apellido']."</span>";
    }
    $result->free_result();
    $mysqli->close();
}

/**
 * Arma el select de modelo segun la id de marca pasada por parametro
 * @param $idMarca
 */
function selectModeloSegunidMarca($idMarca){
    $query = "SELECT * FROM Modelos WHERE idMarca='$idMarca'";
    $idmarca0 = "SELECT * FROM Modelos";
    $mysqli = conectarBD();
    if ($idMarca == 0){
        $result = $mysqli->query($idmarca0);
    }else{
        $result = $mysqli->query($query);
    }
    while ($row = $result->fetch_assoc()){
        echo "<option value=\"".$row['idModelo']."\">".ucwords($row['Modelo'])."</option>";
    }
    $result->free_result();
    $mysqli->close();
}