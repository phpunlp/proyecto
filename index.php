<?php

    require_once('cabecera.php');
    require_once('funciones.php');
    require('consulta.php');
    require('validaGets.php');
    require_once ('modelUsuario.php');
     
?>
<!-- CABECERA DE LA PAGINA -->
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="UTF-8"/>

    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>
</head>
 
<body>
<div class="col-lg-12">
    
    <header class="panel-heading container-fluid">

        <!-- Logo -->
        <div class="col-lg-12">
            <div class="col-lg-6">
                    <a href="index.php">
                <img src="imagenes/logo.png" alt="75" class="img-rounded"></a>
            </div>

            <?php
            if (!isset($_SESSION['usuario'])){
            ?>
            <div class="col-lg-6 left">

                <!-- Div contenedor del formulario de login flotando a derecha del logo -->
                <div class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block">

                    <!-- IMPRIME MENSAJES DE ERROR DESDE JS -->
                    <div class="alert " id="divError" style="margin-bottom: inherit;"></div>

                    <!-- Formulario de login -->
                    <form class="form-inline" action="login.php" method="post" onsubmit="return validaLogin(this)"
                          name="formularioLogin" style="float: right;">

                        <div class="form-group" id="divUsuario">
                            <label for="idUsuario"> Usuario&nbsp;</label>
                            <input type="text" id="idUsuario" name="Usuario" class="form-control"
                                   placeholder="Ingrese su Usuario" onclick="borrarError(document.getElementById('divError'));
                               borrarError(document.getElementById('errorLogin'));"/>
                        </div>
                        <div class="form-group" id="divPassword">
                            <label for="password">&nbsp;&nbsp;&nbsp;Clave&nbsp;</label>
                            <input type="password" id="password" name="Clave" class="form-control"
                                   placeholder="Ingrese Su Contraseña"
                                   onclick="borrarError(document.getElementById('divError'))">
                        </div>

                        <input type="submit" class="btn btn-default" value="Entrar"/>
                    </form>
                    <?php
                    }
                ?>
            </div>
            <!--IMPRIME ERRORES DE EXCEPCION CON LA CLASE Usuario-->
            <?php
            if ((isset($_SESSION['exception'])) && $_SESSION['exception']->getMessage() != ""){
                echo "<div id=\"errorLogin\"class=\"alert alert-danger col-lg-7\" style=\"margin-top: 0%;\">"
                    .$_SESSION['exception']->getMessage()."</div>";
                unset($_SESSION['exception']);

            }
            ?>
        </div>
        </div>
    </header>

    <!-- BARRA DE NAVEGACION -->
    <nav class="container-fluid" >

        <!-- nav-justified para que tengan todas las opciones li el mismo tamaño
        y ocupen toda la caja que la contiene -->
        <ul class="nav nav-tabs nav-justified">
            <!-- active marca el elemento de la lista como activo -->
            <li class="active"><a href="index.php">Principal</a></li>
            <li><a href="info.php">Acerca De:</a></li>
            <li><a href="place.php">Ubicación</a> </li>
            <li><a href="comollegar.php">Como Llegar</a> </li>
            <li><a href="contacto.php">Contacto</a></li>
        </ul>
        <br>
    </nav>




    <!-- MENU DE FILTROS -->
    <div class="col-lg-2">
    <aside class="navbar-text">

            <!-- FORMULARIO DE FILTRADO El mismo se reenvia los filstros
             Filtra y actualiza la lista de vehiculos -->

            <form action="index.php" method="post" name="formFilter">
                <div class="form-group">
                    <label for="filtroTipo">Tipo</label>
                    <select name="Tipo" id="filtroTipo" class="form-control"> <!-- SELECT SELTIPO -->
                    <option  value="Todos">Todos</option>
                    <?php 
                    
                    $mysqli = conectarBD();
                    $query = "SELECT * FROM Tipos ORDER BY Tipo ASC"; 
                    $result = $mysqli->query($query);
                    while($rows = $result->fetch_assoc() )
                        {   
                            if ($rows['Tipo'] == $tipo) {
                                 echo "<option selected value='".$rows['Tipo']."'>".ucwords($rows['Tipo'])."</option>";
                            }else{
                                echo "<option value='".$rows['Tipo']."'>".ucwords($rows['Tipo'])."</option>";
                            }
                           
                        }
                    ?>
                        
                    </select>
                </div>
                
                <div class="form-group" id="divModeloVehiculo">
                    <label for="filtroModelo">Marca y Modelo</label>
                    <select name="Modelo" id="filtroModelo" class="form-control"> <!-- SELECT SELMODELO -->
                        <option  value="Todos">Todos</option>
                    <?php 
                    
                    $query = "SELECT * FROM Modelos AS md INNER JOIN Marcas AS ma ON 
(md.idMarca = ma.idMarca) ORDER BY ma.Marca ASC";
                    $result = $mysqli->query($query);
                    while($rows = $result->fetch_assoc())
                        {
                            if ($rows['Modelo'] == $modelo) {
                                echo "<option selected value='".$rows['Modelo']."'>".ucwords(getMarca($rows['idModelo']))
                                    .' '.ucwords($rows['Modelo'])."</option>";
                            }else{
                            echo "<option value='".$rows['Modelo']."'>".ucwords(getMarca($rows['idModelo'])).' '
                                .ucwords($rows['Modelo'])."</option>"; }
                        }



                    ?>
                    </select>
                </div>

                <!-- LISTA DE CHEQUEO -->
                <div class="list-unstyled" id="divCaractVehiculo">
                    <label>Características</label><br>
                </div>
                <div class="checkbox">
                 <?php 
                    
                    $query = "SELECT * FROM Caracteristicas ORDER BY Caracteristica ASC"; 
                    $result = $mysqli->query($query);
                    while($rows = $result->fetch_array(MYSQLI_ASSOC) )
                        {
                            if($caracteristicas != null){   

                                  if (in_array($rows['idCaracteristica'],$caracteristicas )) {
                                echo '<label><input checked type="checkbox" name="Caracteristica['.$rows['idCaracteristica'].']" value="'.$rows['idCaracteristica'].'">'.ucwords($rows['Caracteristica']).'</label><br>';
                            }else{
                                echo '<label><input type="checkbox" name="Caracteristica['.$rows['idCaracteristica'].']" value="'.$rows['idCaracteristica'].'">'.$rows['Caracteristica'].'</label><br>';
                            }
                        }else{ 
                echo '<label><input type="checkbox" name="Caracteristica['.$rows['idCaracteristica'].']" value="'.$rows['idCaracteristica'].'">'.ucwords($rows['Caracteristica']).'</label><br>';
                            }
                        }

                   ?>                 
                    
                </div>
                <div class=" form-group">
                    <input type="submit" class="btn btn-default" value="Filtrar" id="btnFilters">
                </div>
            </form>
    </aside>
    </div>

    <!-- CUERPO DE LA PAGINA -->
    
    <div class="container pull-right" style="overflow: scroll; height:75% ;  width: 80%;">
    
    
<table  class='sortable table' style="border-color: darkgrey; border-width: 1px;">
    <thead>

      <tr>
        <th class="success">Imagen</th>
        <?php 
         require('getDatos.php');  
         $url='index.php';
      
        ?>
        <th class="success">Marca <span class='glyphicon glyphicon-resize-vertical'></span></th>
        <th class="success">Modelo <span class='glyphicon glyphicon-resize-vertical'></span></th>
        <th class="success">Tipo <span class='glyphicon glyphicon-resize-vertical'></span></th>
        <th class="success">Dominio</th>  
        <th class="success">Precio <span class='glyphicon glyphicon-resize-vertical'></span></th>   
        <th></th>
      </tr>
     </thead> 

       <tbody>
     
      <!-- Aplicadas en las celdas (<td> o <th>) -->
     
                <?php 
                
                if (($query->num_rows)>0) {
                  

                        while($rows = $query->fetch_array() )
                          {
                           
                            echo " <tr>";
                            
                            echo'<td><a href="detalle.php?id='.$rows['idVehiculo'].'"><img src="mostrarImagen.php?idVehiculo='.$rows['idVehiculo'].'" class="img-thumbnail" width=200px heigth=200px /></td>';
                            echo " <td><a href='detalle.php?id=".$rows['idVehiculo']."'>".ucwords(getMarca($rows['idModelo']))."</a></td>";
                            echo " <td>".ucwords(getModelo($rows['idModelo']))."</td>";
                            echo " <td>".ucwords(getTipo($rows['idTipo']))."</td>";
                            echo " <td>".$rows['dominio']."</td>";
                            echo " <td><strong> $ </strong>".$rows['precio']."</td>";
                            echo "<td><a href='detalle.php?id=".$rows['idVehiculo']."'><button class='btn btn-primary btn-lg '><span class='glyphicon glyphicon-shopping-cart'></span>Comprar</button></a></td> ";
                            echo " </tr>";

                          }
              }else{
                echo " <tr>";
                echo "<td>Ningun elemento coincide con el criterio de busqueda</td>";
                echo " </tr>";



              }

                 ?>



      <!-- Aplicadas en las filas -->

       
        
      
    </tbody>
    </table>
 
    

    <!-- PIE DE PAGINA -->
  
    </div>
    </div>
</body>
</html>