<?php
    require_once ('cabecera.php');
    require_once ('funciones.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <title>Ubicación</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/funciones.js" type="text/javascript"></script>
</head>
<body>

<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <div class="col-lg-12">
        <div class="col-lg-6">
            <a href="index.php">
                <img src="imagenes/logo.png" alt="75" class="img-rounded"></a>
        </div>

        <!-- FORMULARIO LOGIN-->
        <?php
        if (!isset($_SESSION['usuario'])){
        ?>
        <div class="col-lg-6 left">
            <!-- Div contenedor del formulario de login flotando a derecha del logo -->
            <div class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block">
                <div class="alert " id="divError" style="margin-bottom: inherit;"></div>
                <!-- Formulario de login -->
                <form class="form-inline" action="login.php" method="post" onsubmit="return validaLogin(this)"
                      name="formularioLogin" style="float: right;">
                    <!-- IMPRIME MENSAJES DE ERROR DESDE JS -->
                    <div class="form-group" id="divUsuario">
                        <label for="idUsuario"> Usuario&nbsp;</label>
                        <input type="text" id="idUsuario" name="Usuario" class="form-control"
                               placeholder="Ingrese su Usuario" onclick="borrarError(document.getElementById('divError'));
                               borrarError(document.getElementById('errorLogin'));"/>
                    </div>
                    <div class="form-group" id="divPassword">
                        <label for="password">&nbsp;&nbsp;&nbsp;Clave&nbsp;</label>
                        <input type="password" id="password" name="Clave" class="form-control"
                               placeholder="Ingrese Su Contraseña"
                               onclick="borrarError(document.getElementById('divError'))">
                    </div>

                    <input type="submit" class="btn btn-default" value="Entrar"/>
                </form>
            </div>
            <?php
            }
            if ((isset($_SESSION['exception'])) && $_SESSION['exception']->getMessage() != ""){
                echo "<div id=\"errorLogin\"class=\"alert alert-danger col-lg-7\" style=\"margin-top: 0%;\">"
                    .$_SESSION['exception']->getMessage()."</div>";
                unset($_SESSION['exception']);

            }
            ?>
        </div>
    </div>
</header>

<!-- BARRA DE NAVEGACION -->
<nav class="container-fluid">

    <!-- nav-justified para que tengan todas las opciones li el mismo tamaño
    y ocupen toda la caja que la contiene -->
    <ul class="nav nav-tabs nav-justified">
        <!-- active marca el elemento de la lista como activo -->
        <li><a href="index.php">Principal</a></li>
        <li><a href="info.php">Acerca De:</a></li>
        <li class="active"><a href="place.php">Ubicación</a> </li>
        <li><a href="comollegar.php">Como Llegar</a> </li>
        <li><a href="contacto.php">Contacto</a></li>
    </ul>

</nav>
<!-- CUERPO DE LA PAGINA -->
<section class="panel-body>">
    <article class="container-fluid" style="margin-left: 25%">
        <h4 class="text-primary">
            Ubicación:
        </h4>
        <iframe  src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d2183.6615129801953!2d-57.97059604283775!3d
        -34.91157895198006!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sar!4v1473111960505"
                width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </article>
</section>
<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>