<?php
/* Si no se inicio sesion, se redirije a index.php */
    require_once ('cabecera.php');
    require_once ('funciones.php');
    checkLogin();
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar-left">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default active" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <div style="width: 80%;"  class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <?php if ((empty($_POST)) || (!empty($_GET))) { ?>
            <h4 class="text-primary">
                Dar de Baja un Vehiculo
            </h4>
            <div class="has-error" id="divError"></div> <!-- DIV DONDE APARECE EL ERROR -->

                                                        <!-- FORMULARIO bajaVehiculo (SE REENVIA ASI MISMO EL FORMULARIO-->
            <form class="form-horizontal" action="listadoBajaVehiculo.php" method="post"
                  onsubmit="return validaBajaMyM()">
                <div class="form-group" id="divSelMarca">
                    <label for="selMarca">Seleccione Marca:</label>
                    <select class="form-control" id="selMarca" name="selectMarca"
                            onchange="location.href='bajaVehiculo.php?selectMarca='+this.value">
                        <?php
                        if (empty($_GET)) { ?>
                            <option selected value="0">Ninguno</option>
                            <?php selectMarca();
                        } else { ?>
                            <option value="0">Ninguno</option>
                        <?php selectedMarca($_GET['selectMarca']); } ?>
                    </select>
                </div>

                <div class="form-group" id="divSelMod">
                    <label for="selMod">Seleccione Modelo:</label>
                    <select class="form-control" id="selMod" name="selectModelo"
                            onchange="setDivFormGroup(document.getElementById('divSelMod'))">
                        <option selected value="0">Ninguno</option>
                        <?php
                        selectModeloSegunidMarca($_GET['selectMarca']);
                        ?>
                    </select>
                </div>

                <!-- HACER CON PHP QUE SE MUESTREN TODOS LOS DATOS DE DICHO VEHICULO -->
                <input type="submit" class="btn btn-default" value="Enviar">
            </form>
            <?php
        }
        ?>
    </div>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>