<?php
require_once ('cabecera.php');
require_once('funciones.php');
checkLogin();
?>
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar-left">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default active" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <div style="width: 80%;" class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <h4 class="text-primary">
            Modificar un Vehiculo
        </h4>
        <?php if (!empty($_GET['error']) && empty($_POST)) {
            if (isset($_GET['error']) === false) {
                errorMessage("No se pudo eliminar el vehiculo intente nuevamente");
            }
            if (isset($_GET['error']) === true) {
                successMessage("El vehiculo fue eliminado correctamente");
            }
        }
        ?>
        <div class="has-error" id="divError"></div> <!-- DIV DONDE APARECE EL ERROR -->

        <!-- LISTADO DONDE SE MUESTRA EL LISTADO DE VEHICULOS DE DICHO MODELO PARA SELECCIONAR UNO Y DARLO DE BAJA -->
        <div class="table-responsive">

            <!-- table-bordered (arma la tabla con bordes) table-hover(ilumina con hover donde esta el cursor -->
            <table class="table  table-bordered text-center"> <!-- esta debe cargarse dinamicamente con PHP -->
                <thead> <!-- cabecera de la tabla -->
                <tr>
                    <th>Modelo</th>
                    <th>Marca</th>
                    <th>Año</th>
                    <th>Precio</th>
                    <th>Tipo</th>
                    <th>Dominio</th>
                </tr>
                </thead>
                <tbody> <!-- cuerpo de la tabla -->
                <?php
                if ((isset($_POST['selectMarca'])) && (isset($_POST['selectModelo']))) {
                    cargaTablaModiVehiculo($_POST['selectModelo'], $_POST['selectMarca']);
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>