<?php
    require_once ('cabecera.php');
    require_once ('funciones.php');
    checkLogin();
?>
<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default active" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <article style="width: 80%;" class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <?php
        if ((empty($_POST)) || (!empty($_GET))) {
            ?>
            <h4 class="text-primary">
                Modificar Marca o Modelo de Vehiculo
            </h4>

            <!-- Div donde mostrar error del formulario -->
            <div id="divError"></div>

            <!-- FORMULARIO PARA DAR DE ALTA UN NUEVO TIPO DE VEHICULO (SE REENVIA ASI MISMO EL FORMULARIO-->
            <form class="form-horizontal" action="modiMarca.php" method="post" onsubmit="return validaModiMarca()">

                <!-- Select Marca -->
                <div class="form-group" id="divSelMarca" onchange="setDivFormGroup(this)">
                    <label for="selMarca">Seleccione Marca a Modificar</label>
                    <select class="form-control" id="selMarca" name="selectMarca"
                            onchange="borrarError(document.getElementById('divError'));
                            ocultaMuestraInput(this,document.getElementById('divNewMarca'));
                            setDivFormGroup(document.getElementById('divSelMod'));location.href='http://grupo36.php.info.unlp.edu.ar/modiMarca.php?selectMarca='+this.value;">

                        <?php
                        if (empty($_GET['selectMarca'])){
                            ?> <option selected value="0">Modificar Solo Modelo</option>
                        <?php
                            selectMarca();
                        }else{ ?>
                            <option value="0">Modificar Solo Modelo</option>
                        <?php
                            selectedMarca($_GET['selectMarca']);
                        }
                        ?>

                    </select>
                </div>

                <!-- INPUT NUEVO NOMBRE DE MARCA -->
                <div class="form-group" id="divNewMarca" <?php if (empty($_GET['selectMarca'])){echo "hidden";} ?>
                     onchange="setDivFormGroup(this)">
                    <label for="nomMarca">Ingrese Nuevo nombre para la Marca</label>
                    <input type="text" id="nomMarca" name="nombreMarca" class="form-control"
                           placeholder="Ingrese Nuevo nombre Para la marca seleccionada"
                           onclick="borrarError(document.getElementById('divError'))">
                </div>

                <!-- SELECT MODELO EXISTENTE -->
                <div class="form-group" id="divSelMod" onchange="setDivFormGroup(this)">
                    <label for="selNomMod">Seleccione un modelo a Modificar</label>
                    <select id="selNomMod" class="form-control" name="selectModelo"
                            onchange="borrarError(document.getElementById('divError'));
                        ocultaMuestraInput(this,document.getElementById('divNewModel'));
                        setDivFormGroup(document.getElementById('divSelMarca'))">
                        <option selected value="0">Modificar Solo Marca</option>
                        <?php
                        selectModeloSegunidMarca($_GET['selectMarca']);
                        ?>
                    </select>
                </div>

                <!-- DIV NUEVO MODELO -->
                <div class="form-group" id="divNewModel" hidden onclick="setDivFormGroup(this)">
                    <label for="nomModelo">Nombre Del Modelo:</label>
                    <input class="form-control" type="text" id="nomModelo" name="nombreModelo"
                           placeholder="Ingrese una Marca de Vehiculo"
                           onclick="borrarError(document.getElementById('divError'))">
                </div>
                <input type="submit" class="btn btn-default" value="Enviar">
            </form>
            <?php
        }else {
            //si nombreMarca y nombreModelo estan definidos se modifica marca y modelo
            if ((isset($_POST['nombreMarca'])) && (validaText($_POST['nombreMarca'])) &&
                (isset($_POST['nombreModelo'])) && (validaText($_POST['nombreModelo'])) &&
                ($_POST['selectMarca'] > 0) && ($_POST['selectModelo'] > 0)
            ) {
                modificarMarca($_POST['nombreMarca'], $_POST['selectMarca']);
                modificarModelo($_POST['nombreModelo'], $_POST['selectModelo']);

            } elseif ((isset($_POST['nombreMarca'])) && (validaText($_POST['nombreMarca'])) &&
                (empty($_POST['nombreModelo']))){ //Solo modifico la marca
                modificarMarca($_POST['nombreMarca'], $_POST['selectMarca']);

            } elseif ((isset($_POST['nombreModelo'])) && (validaText($_POST['nombreModelo'])) &&
                (empty($_POST['nombreMarca']))){
                modificarModelo($_POST['nombreModelo'], $_POST['selectModelo']);
            }
        }
        ?>
    </article>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>