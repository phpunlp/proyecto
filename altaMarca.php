<?php
/* Si no se inicio sesion, se redirije a indexPublico.php */
require_once ('cabecera.php');
require_once ('funciones.php');
checkLogin();
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="js/funciones.js"></script>
    <title>WebCar</title>

</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <a href="index.php"><img class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block"
                             src="imagenes/logo2.jpg"/></a>

</header>

<!-- MENU DE OPCIONES -->
<aside class="navbar-text navbar-left">

    <!-- Alta Baja Y Modificacion de Tipos de Vehiculos -->
    <!-- Cada boton envia mediante un GET codigoModificacion modificando dinamicamente el cuerpo
    de la pagina -->
    <p class="text-info text-center">Tipo</p>
    <div class="btn-group"> <!-- DIV PARA AGRUPAR LOS BOTONES -->
        <a class="btn btn-default" href="altaTipo.php">Alta</a>
        <a class="btn btn-default" href="bajaTipo.php">Baja</a>
        <a class="btn btn-default" href="modifType.php">Modificación</a>
    </div>
    <!-- Alta Baja Y Modificacion de Marcas Vehiculos -->
    <p class="text-info text-center">Marca y Modelo</p>
    <div class="btn-group">
        <a class="btn btn-default active" href="altaMarca.php">Alta</a>
        <a class="btn btn-default" href="bajaMarca.php">Baja</a>
        <a class="btn btn-default" href="modiMarca.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Caracteristicas -->
    <p class="text-info text-center">Características</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaCaracteristica.php">Alta</a>
        <a class="btn btn-default" href="bajaCaracteristica.php">Baja</a>
        <a class="btn btn-default" href="modiCaracteristica.php">Modificación</a>
    </div>

    <!-- Alta Baja y Modificacion de Vehiculos -->
    <p class="text-info text-center">Vehículos</p>
    <div class="btn-group">
        <a class="btn btn-default" href="altaVehiculo.php">Alta</a>
        <a class="btn btn-default" href="bajaVehiculo.php">Baja</a>
        <a class="btn btn-default" href="modiVehiculo.php">Modificación</a>
    </div>

</aside>

<!-- CUERPO DE LA PAGINA -->
<section class="panel-body">
    <div style="width: 80%;" class="container text-center visible-lg-inline-block visible-md-inline-block visible-sm-inline-block">
        <?php
            if (empty($_POST)) { //Si no se envio el formulario se muestra
                ?>
                <h4 class="text-primary">
                    Dar de Alta Una Nueva Marca o Modelo de Vehiculo
                </h4>
                <div class="has-error" id="error"></div> <!-- DIV DONDE APARECE EL ERROR -->
                                                         <!-- FORMULARIO MODIFICAR MARCA Y MODELO DE VEHICULO (SE REENVIA ASI MISMO EL FORMULARIO-->
                <form class="form-horizontal" action="altaMarca.php" method="post"
                      onsubmit="return validaNewMarcaModel()">
                    <div class="form-group" id="selMarca">
                        <label for="selectMarca">Seleccione Marca Existente</label>
                        <select class="form-control in" name="selMarca" id="selectMarca"
                                onchange="muestraOcultaInput(this,document.getElementById('divNomMarca'))">
                            <option selected value="0">Otra Marca</option>
                            <?php
                                selectMarca();
                            ?>
                        </select>
                    </div>
                    <!-- SOLO SE MUESTRA SI EL SELECT ESTA SELECCIONADO EN OTRO -->
                    <div class="form-group" id="divNomMarca" onclick="setDivFormGroup(this)">
                        <label for="nomMarca">Nombre De La Nueva Marca:</label>
                        <input class="form-control" type="text" id="nomMarca" name="nombreMarca"
                               placeholder="Ingrese una Marca de Vehiculo">
                    </div>

                    <div class="form-group" id="divModelo" onclick="setDivFormGroup(this)">
                        <label for="nomModelo">Nombre del Modelo</label>
                        <input type="text" class="form-control" id="nomModelo" name="nombreModelo"
                               placeholder="Ingrese Modelo del Vehiculo"
                               onclick="setDivFormGroup()">
                    </div>

                    <input type="submit" class="btn btn-default" value="Enviar">
                </form>
                <?php
            }else{
                /* Si selecciono una marca existente agrega el modelo a dicha marca */
                if ((isset($_POST['nombreModelo'])) && ($_POST['selMarca'] > 0)){
                   if (($_POST['nombreModelo'])){
                       $modelo = strtolower($_POST['nombreModelo']);
                       $idMarca = strtolower($_POST['selMarca']);
                       altaModelo($modelo,$idMarca);
                   }else{
                       errorMessage("Nombre de Modelo invalido, Verfique");
                   }
                   /*Si selecciona otra marca y existen los dos input marca y modelo, doy de alta la marca y el modelo */
                }else if ((isset($_POST['nombreModelo'])) && (isset($_POST['nombreMarca']))){
                    if (($_POST['nombreModelo']) && validaText($_POST['nombreMarca'])){
                        $modelo = strtolower($_POST['nombreModelo']);
                        $marca = strtolower($_POST['nombreMarca']);
                        /* Da de alta primero la marca, y luego mediante la funcion ultimoID toma el id de dicha insercion
                        Para luego utilizarlo para dar de alta el modelo */
                        $idNewMarca = altaMarca($marca);
                        if ($idNewMarca >= 0){
                            altaModelo($modelo,$idNewMarca);//Si no existe, da de alta en la BD el modelo de la marcaID
                        }else{
                            errorMessage("Al ocurrir un error en la marca, no se pudo continuar, intente nuevamente");
                        }
                    }
                    }
            }
        ?>
    </div>
</section>

<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>