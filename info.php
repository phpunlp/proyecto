<?php
    require_once("cabecera.php");
    require_once('funciones.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <title>Acerca de WebCar</title>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <script src="js/funciones.js" type="text/javascript"></script>
</head>
<body>
<!-- CABECERA DE LA PAGINA -->
<header class="panel-heading container-fluid">

    <!-- Logo -->
    <div class="col-lg-12">
        <div class="col-lg-6">
            <a href="index.php">
                <img src="imagenes/logo.png" alt="75" class="img-rounded"></a>
        </div>

        <!--FORMULARIO LOGIN-->
        <?php
        if (!isset($_SESSION['usuario'])){
        ?>
        <div class="col-lg-6 left">
            <!-- Div contenedor del formulario de login flotando a derecha del logo -->
            <div class="visible-sm-inline-block visible-lg-inline-block visible-md-inline-block">
                <div class="alert " id="divError" style="margin-bottom: inherit;"></div>
                <!-- Formulario de login -->
                <form class="form-inline" action="login.php" method="post" onsubmit="return validaLogin(this)"
                      name="formularioLogin" style="float: right;">
                    <!-- IMPRIME MENSAJES DE ERROR DESDE JS -->
                    <div class="form-group" id="divUsuario">
                        <label for="idUsuario"> Usuario&nbsp;</label>
                        <input type="text" id="idUsuario" name="Usuario" class="form-control"
                               placeholder="Ingrese su Usuario" onclick="borrarError(document.getElementById('divError'));
                               borrarError(document.getElementById('errorLogin'));"/>
                    </div>
                    <div class="form-group" id="divPassword">
                        <label for="password">&nbsp;&nbsp;&nbsp;Clave&nbsp;</label>
                        <input type="password" id="password" name="Clave" class="form-control"
                               placeholder="Ingrese Su Contraseña"
                               onclick="borrarError(document.getElementById('divError'))">
                    </div>

                    <input type="submit" class="btn btn-default" value="Entrar"/>
                </form>
            </div>
            <?php
            }
            if ((isset($_SESSION['exception'])) && $_SESSION['exception']->getMessage() != ""){
                echo "<div id=\"errorLogin\"class=\"alert alert-danger col-lg-7\" style=\"margin-top: 0%;\">"
                    .$_SESSION['exception']->getMessage()."</div>";
                unset($_SESSION['exception']);

            }
            ?>
        </div>
    </div>
</header>

<!-- BARRA DE NAVEGACION -->
<nav class="container-fluid">

    <!-- nav-justified para que tengan todas las opciones li el mismo tamaño
    y ocupen toda la caja que la contiene -->
    <ul class="nav nav-tabs nav-justified">
        <!-- active marca el elemento de la lista como activo -->
        <li><a href="index.php">Principal</a></li>
        <li class="active"><a href="info.php">Acerca De:</a></li>
        <li><a href="place.php">Ubicación</a> </li>
        <li><a href="comollegar.php">Como Llegar</a> </li>
        <li><a href="contacto.php">Contacto</a></li>
    </ul>

</nav>
<!-- CUERPO DE LA PAGINA -->
<section class="panel-body>">
    <article class="container">
        <h3 class="text-primary">
        <br>
        <br>
          Nuestra historia
        </h3>

        <p class="text-primary">
            El Grupo WebAuto nace como aplicacion web dedicada a la comercialización de vehículos 0km y Usados.
            Su carácter familiar ha constituido a lo largo de su trayectoria, una fortaleza central al competir en un mundo global y cambiante.
        </p>

         <p class="text-primary">
            En el año 1972 inicia su actividad , apostando de esta manera al comercio automotor,
            con el objetivo claro de atender cada día mejor a nuestros clientes.
        </p>

        <p class="text-primary">
            En 1991 comenzamos con la primera concesión de Motores Diesel,
            consiguiendo al año el segundo puesto a nivel nacional de ventas de motores Perkins.
        </p>

         <p class="text-primary">
            Hacia el año 1992, y dada la posibilidad de importar vehículos 0km, la empresa participó activamente en este
            negocio llegando a importar entre 1992 y 1994 alrededor de 2000 unidades 0km procedentes de Francia
            (Renault y Peugeot) y de Italia ( Fiat) convirtiéndose en uno de los mayores
            importadores de vehículos de esos años.
        </p>

         <p class="text-primary">
            A fines de 1994, luego del cierre de las importaciones y gracias a la experiencia recogida se alcanzó el
            nombramiento como concesionario oficial de las marcas Mitsubishi, Kia, Mazda y Asia.
        </p>

        <p class="text-primary"> En 1997 cumplió el gran sueño de lograr la concesionaria Oficial Renault
            situada en 17 y 35.Desde ese momento ha experimentado un crecimiento sostenido, constituyéndose en un
            referente de la marca Renault en la ciudad de La Plata y en el país.
        </p>

          <p class="text-primary">
             Tres años después, en el año 2000, se inauguró Jachifa Automotora, Concesionario Oficial Chevrolet.
             Actualmente se destaca por su gran caudal de ventas que la posicionan entre las primeras
             8 concesionarias de la República Argentina.
         </p>

         <p class="text-primary">
             Posteriormente a estos avances no solo empresariales sino económicos, se decide abrir en el 2016
             "webCar, paseo del Usado", vinculando lo real con lo virtual a través de su gran portal
             de internet www.webAuto.com.ar.
         </p>

        <p class="text-primary">
            La concesión de Fiat llego en el año 2005 abriendo sus puertas en Camino Centenario y 438 y
            posicionándose como líder de ventas de la marca en nuestra ciudad.
        </p>

         <p class="text-primary">
            Sin perder de vista sus orígenes el Grupo Randazzo decidió seguir impulsando la comercialización de
            autos usados y es así como en Septiembre de 2007 se presento el primer predio de
            ventas de autos de la Ciudad con el respaldo Oficial Renault, "Renault Ocasión".
        </p>

        <p class="text-primary">
            Siguiendo la filosofía del Grupo otorgar excelencia en servicios y calidad,
            en el 2009 se logra abrir dos centros exclusivos de servicios Chevrolet y Renault que hoy atiende a gran
            parte del parque automotor de la ciudad. Orientados siempre hacia la satisfacción total del cliente,
            el nuevo centro de Calidad y Servicios cuenta con la tecnología requerida de acuerdo a normas
            internacionales de las marcas y con un equipo humano conformado por técnicos y profesionales
            altamente calificados.
        </p>

         <p class="text-primary">
             El desarrollo de la Postventa, ha sido fundamental en los últimos años. Hoy, nuestros equipos están
             formados por Asesores de Servicios capacitados no sólo técnicamente, sino también en materia de
             comunicación, negociación y ventas, con el fin alcanzar los niveles de facturación que nos hemos propuesto.
             El equipo de mecánicos, formados para dar respuesta a los diferentes requerimientos de las marcas,
             trabajan con la motivación de solucionar el problema en la primera vez, con el apoyo de especialistas
             altamente calificados, que acompañan al cliente para realizar el correspondiente "Chequeo de Calidad".
        </p>
    </article>
</section>
<!-- PIE DE PAGINA -->
<footer class="panel-footer">
    <div class="container-fluid">
        <p><?php include('footer.php'); ?></p>
    </div>
</footer>
</body>
</html>