<?php
require_once "conexion.php";
require_once "funciones.php";

if (isset( $_GET['idVehiculo'] )) {
    $idVehiculo = $_GET['idVehiculo'];
    $query = "SELECT contenidoimagen,tipoimagen FROM Vehiculos WHERE idVehiculo='$idVehiculo'";
    $mysqli = conectarBD();
    $result = $mysqli->query($query)->fetch_assoc();
    $mysqli->close();

//Se le avisa al navegador que es un archivo en particular
    if ($result['tipoimagen'] === IMAGETYPE_JPEG) {
        header("Content-Type: image/jpeg");
    } elseif ($result['tipoimagen'] === IMAGETYPE_PNG) {
        header("Content-Type: image/png");
    }
    echo $result['contenidoimagen'];
}else{
    errorMessage("Error id de imagen no valido.");
}