<?php
    require_once ("conexion.php");
    require_once ("funciones.php");
/**
 * Created by PhpStorm.
 * User: simch
 * Date: 23/11/2016
 * Time: 10:34
 */
class Usuario{
    private $email;
    private $password;
    private $id;
    private $nombre;
    private $apellido;
    private $telefono;
    private $logueado;

    /**
     * Usuario constructor.
     * Se construye con el $email y $password, el resto de los atributos del usuario son generados por defecto
     * En valores nulos, Una vez logueado el usuario, Carga todos los demas parametros faltantes con los datos
     * de la BD de dicho usuario, Y retorna una excepcion en caso de que no se haya podido validar el email
     * y la contraseña
     * @param $email
     * @param $password
     * @param int $id
     * @param string $nombre
     * @param string $apellido
     * @param string $telefono
     * @param boolean $logueado
     */
    public function __construct($email, $password, $id=0, $nombre="", $apellido="", $telefono="",$logueado=false){
        $this->email = $email;
        $this->password = $password;
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->telefono = $telefono;
        $this->logueado = $logueado;
    }

    public function login(){
        $query = "SELECT * FROM usuarios WHERE email = '$this->email' AND clave = '$this->password'";
        try {
            $mysqli = conectarBD();
        }catch (Exception $e){
            echo $e->getMessage();
        }
            //Si hay resultados en la BD es porque son correctos el password y el email
            $result = $mysqli->query($query);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $this->setId($row['idUsuario']);
                $this->setNombre($row['nombre']);
                $this->setApellido($row['apellido']);
                $this->setTelefono($row['telefono']);
                $this->setLogueado(true);
                $result->free_result();
                $mysqli->close();
            } else { //Si no hay resultados el usuario o contraseña es incorrecto Lanzo excepcion
                throw new Exception("Usuario o Contraseña incorrecta, Por Favor intentelo nuevamente");
            }
    }

    public function cerrarSession(){
        session_destroy();
}
    /**
     * @return mixed
     */
    public function getLogueado()
    {
        return $this->logueado;
    }

    /**
     * @param mixed $logueado
     */
    public function setLogueado($logueado)
    {
        $this->logueado = $logueado;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

}